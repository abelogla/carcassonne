package features;

import java.util.ArrayList;

import carcassonne.Feature;
import carcassonne.Follower;
import carcassonne.Tile;

public class Road extends Feature {

	private int _opennings = 0;
	private ArrayList<Tile> _tiles = new ArrayList<Tile>();
	private ArrayList<Feature> _sameFeatures = new ArrayList<Feature>();


	public Road(Tile t, int i) {
		_opennings = i;
		_sameFeatures.add(this);
		_tiles.add(t);
	}

	@Override
	public int getOpennings() {
		return _opennings;
	}

	@Override
	public void setOpennings(int opennings) {
		_opennings = opennings;
	}

	@Override
	public ArrayList<Tile> getTiles() {
		return _tiles;
	}

	@Override
	public void setTiles(ArrayList<Tile> tiles) {
		_tiles = tiles;

	}

	@Override
	public void addTiles(ArrayList<Tile> tiles) {
		for(Tile t:tiles) {
			if(!_tiles.contains(t)) {
				_tiles.add(t);
			}
		}
		for(Feature f:_sameFeatures) {
			f.setTiles(_tiles);
		}
	}

	@Override
	public ArrayList<Follower> getFollowers() {
		return _followers;
	}

	@Override
	public void setFollowers(ArrayList<Follower> followers) {
		_followers.addAll(followers);
	}

	@Override
	public void setAllFollowers(ArrayList<Follower> followers) {
		_followers = followers;
	}

	@Override
	public void setFollowersOnAllSameFeatures() {
		for(Feature f:_sameFeatures) {
			f.setAllFollowers(_followers);
		}
	}

	@Override
	public int getNumberOfTiles() {
		return _tiles.size();
	}

	@Override
	public ArrayList<Feature> getSameFeatures() {
		return _sameFeatures;
	}

	@Override
	public void setSameFeatures(ArrayList<Feature> sameFeatures) {
		for(int i=0; i<sameFeatures.size(); i++) {
			if(!_sameFeatures.contains(sameFeatures.get(i))) {
				_sameFeatures.add(sameFeatures.get(i));
			}
		}
	}

	@Override
	public void updateNeighbors() {
		for(Feature f:_sameFeatures) {
			f.setSameFeatures(_sameFeatures);
		}
	}

	@Override
	public void updateOpennings(Tile t, int i) {
		for(Feature f:_sameFeatures) {
			if(_tiles.contains(t))
				f.setOpennings(_opennings-2);
			else f.setOpennings(i);
		}
	}

	@Override
	public int returnScore() {
		return _tiles.size();
	}
}
