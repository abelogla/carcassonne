package bag;

import carcassonne.Tile;
import tiles.*;

public class TileGenerator {
	
	public Tile generateTile(String tile) {
		Tile createdTile = new Tile();
		int rotation = (tile.length() == 2) ? tile.charAt(1)-'0' : tile.charAt(2)-'0';
		String type = (tile.length() == 2) ? tile.substring(0, 1) : tile.substring(0, 2);
		switch(type) {
		case "A":
			createdTile = new A();
			break;
		case "B":
			createdTile = new B();
			break;
		case "C":
			createdTile = new C();
			break;
		case "D":
			createdTile = new D();
			break;
		case "E":
			createdTile = new E();
			break;
		case "F":
			createdTile = new F();
			break;
		case "G":
			createdTile = new G();
			break;
		case "H":
			createdTile = new H();
			break;
		case "I":
			createdTile = new I();
			break;
		case "J":
			createdTile = new J();
			break;
		case "K":
			createdTile = new K();
			break;
		case "L":
			createdTile = new L();
			break;
		case "M":
			createdTile = new M();
			break;
		case "N":
			createdTile = new N();
			break;
		case "P":
			createdTile = new P();
			break;
		case "Q":
			createdTile = new Q();
			break;
		case "S":
			createdTile = new S();
			break;
		case "T":
			createdTile = new T();
			break;
		case "U":
			createdTile = new U();
			break;
		case "V":
			createdTile = new V();
			break;
		case "W":
			createdTile = new W();
			break;
		case "X":
			createdTile = new X();
			break;
		case "Y":
			createdTile = new Y();
			break;
		case "Z":
			createdTile = new Z();
			break;
		case "RA":
			createdTile = new RA();
			break;
		case "RB":
			createdTile = new RB();
			break;
		case "RC":
			createdTile = new RC();
			break;
		case "RD":
			createdTile = new RD();
			break;
		case "RE":
			createdTile = new RE();
			break;
		case "RF":
			createdTile = new RF();
			break;
		case "RG":
			createdTile = new RG();
			break;
		case "RH":
			createdTile = new RH();
			break;
		case "RI":
			createdTile = new RI();
			break;
		}
		for(int i=0; i<rotation;i++) createdTile.rotate();
		return createdTile;
	}
}
