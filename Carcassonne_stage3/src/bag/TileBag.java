package bag;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import tiles.*;
import carcassonne.Tile;

public class TileBag {
	/**
	 * Holds the reference to the ArrayList containing the Tile objects left in the "bag".
	 */
	private ArrayList<Tile> _bag;
	private ArrayList<Tile> _riverBag;
	/**
	 * This constructor instantiates the correct number of tiles of each type, shuffles their order,
	 * and puts them into an ArrayList for future access.
	 */
	public TileBag(){
		_bag = new ArrayList<Tile>();
		_riverBag = new ArrayList<Tile>();
		_bag.add(new F());
		_bag.add(new H());
		_bag.add(new J());
		_bag.add(new Q());
		_bag.add(new Z());
		for (int i=0;i<2;i++){
			_bag.add(new B());
			_bag.add(new E());
			_bag.add(new G());
			_bag.add(new P());
			_bag.add(new V());
			_bag.add(new S());
		}
		for (int i=0;i<3;i++){
			_bag.add(new C());
			_bag.add(new D());
			_bag.add(new I());
			_bag.add(new L());
			_bag.add(new M());
			_bag.add(new N());
			_bag.add(new T());
		}
		for (int i=0;i<4;i++){
			_bag.add(new K());
			_bag.add(new U());
			_bag.add(new Y());
		}
		for (int i=0;i<5;i++){
			_bag.add(new A());
		}
		for (int i=0;i<8;i++){
			_bag.add(new W());
		}
		for (int i=0;i<9;i++){
			_bag.add(new X());
		}
		for (int i =0; i < 2; i++){
			_riverBag.add(new RB());
			_riverBag.add(new RC());
		}
		_riverBag.add(new RA());
		_riverBag.add(new RD());
		_riverBag.add(new RE());
		_riverBag.add(new RF());
		_riverBag.add(new RG());
		_riverBag.add(new RH());
		_riverBag.add(new RI());
		
		shuffle();
	}
	
	public TileBag(String[] tiles) {
		_bag = new ArrayList<Tile>();
		_riverBag = new ArrayList<Tile>();
		TileGenerator tg = new TileGenerator();
		System.out.println("Filling up the bag with remaining tiles...");
		for(int i=0; i<tiles.length; i++) {
			Tile newTile = tg.generateTile(tiles[i]);
			if(newTile.getClass().getSimpleName().length() == 1)
				_bag.add(newTile);
			else _riverBag.add(newTile);
		}
		shuffle();
		System.out.println("Tiles in the bag: "+_bag.size());
		System.out.println("Tiles in the river bag: "+_riverBag.size());
	}
	
	private void shuffle() {
		Collections.shuffle(_bag);
		Collections.shuffle(_riverBag);
	}
	
	/**
	 * This method is used by the Model to draw and remove the next tile from the "bag."
	 * @return tile of type Tile that corresponds to the next Tile that has just been removed from the bag.
	 */
	public Tile getTile(){
		Tile drawn = _riverBag.isEmpty() ? _bag.remove(0) : _riverBag.remove(0);
		if(!_riverBag.isEmpty() && drawn.tileName().equals("RA0")) {
			takeTile(drawn);
			drawn = getTile();
		}
		return drawn;
	}
	
	public void takeTile(Tile tile) {
		if(tile.getClass().getSimpleName().length() == 1)
			_bag.add(tile);
		else _riverBag.add(tile);
	}
	
	public String getRemainTiles() {
		String out="";
		for(int i=0; i<_riverBag.size(); i++) {
			out += _riverBag.get(i).tileName()+",";
		}
		for(int i=0; i<_bag.size(); i++) {
			out += _bag.get(i).tileName()+",";
		}
		return out;
	}
}
