package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.River;
import features.Road;

public class RH extends Tile {
	
	public RH() {
		
		Field f1 = new Field();
		Field f2 = new Field();
		Field f3 = new Field();
		Road r = new Road(this,2);
		River ri = new River();
		
		Feature[] top = new Feature[]{f1,ri,f2};
		Feature[] left = new Feature[]{f1,r,f3};
		Feature[] bottom = new Feature[]{f3,r,f1};
		Feature[] right = new Feature[]{f2,ri,f1};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.2,0.2,f1};
		Object[] a2 = new Object[]{0.2,0.85,f3};
		Object[] a3 = new Object[]{0.2,0.5,r};
		Object[] a4 = new Object[]{0.85,0.2,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RH1.png",
				"/pictures/RH2.png",
				"/pictures/RH3.png",
				"/pictures/RH4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
