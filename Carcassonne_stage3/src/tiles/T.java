package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.Road;

public class T extends Tile {
	public T(){
		Field f1 = new Field();
		Field f2 = new Field();
		Field f3 = new Field();
		City c = new City(this,1);
		Road r1 = new Road(this,1);
		Road r2 = new Road(this,1);
		Road r3 = new Road(this,1);
		Feature[] right = new Feature[]{f1,r1,f2};
		Feature[] left = new Feature[]{f1,r2,f3};
		Feature[] bottom = new Feature[]{f3,r3,f2};
		Feature[] top = new Feature[]{c,c,c};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.1,0.25,f1};
		Object[] a2 = new Object[]{0.85,0.85,f2};
		Object[] a3 = new Object[]{0.15,0.85,f3};
		Object[] a4 = new Object[]{0.45,0.1,c};
		Object[] a5 = new Object[]{0.75,0.45,r1};
		Object[] a6 = new Object[]{0.2,0.45,r2};
		Object[] a7 = new Object[]{0.45,0.8,r3};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4,a5,a6,a7};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/T1.png",
				"/pictures/T2.png",
				"/pictures/T3.png",
				"/pictures/T4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
