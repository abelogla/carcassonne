package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.Road;

public class X extends Tile {
	public X(){
		Field f1 = new Field();
		Field f2 = new Field();
		Road r = new Road(this,2);
		Feature[] top = new Feature[]{f1,f1,f1};
		Feature[] right = new Feature[]{f1,f1,f1};
		Feature[] bottom = new Feature[]{f2,r,f1};
		Feature[] left = new Feature[]{f1,r,f2};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.75,0.75,f1};
		Object[] a2 = new Object[]{0.4,0.6,r};
		Object[] a3 = new Object[]{0.15,0.85,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/X1.png",
				"/pictures/X2.png",
				"/pictures/X3.png",
				"/pictures/X4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
