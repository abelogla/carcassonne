package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;

public class I extends Tile {
	public I(){
		Field f = new Field();
		City c = new City(this,3);
		Feature[] top = new Feature[]{c,c,c};
		Feature[] left = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{f,f,f};
		Feature[] right = new Feature[]{c,c,c};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.5,c};
		Object[] a2 = new Object[]{0.5,0.85,f};
		Object[][] coordinates = new Object[][]{a1,a2};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/I1.png",
				"/pictures/I2.png",
				"/pictures/I3.png",
				"/pictures/I4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
