package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.River;
import features.Road;

public class RI extends Tile {
	
	public RI() {
		City c = new City(this,1);
		River ri = new River();
		Road r = new Road(this,1);
		Field f1 = new Field();
		Field f2 = new Field();
		Field f3 = new Field();
		Field f4 = new Field();
		
		Feature[] top = new Feature[]{f1,ri,f2};
		Feature[] left = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{f3,ri,f4};
		Feature[] right = new Feature[]{f2,r,f4};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.1,0.5,c};
		Object[] a2 = new Object[]{0.3,0.1,f1};
		Object[] a3 = new Object[]{0.8,0.2,f2};
		Object[] a4 = new Object[]{0.2,0.85,f3};
		Object[] a5 = new Object[]{0.8,0.85,f4};
		Object[] a6 = new Object[]{0.45,0.45,r};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4,a5,a6};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RI1.png",
				"/pictures/RI2.png",
				"/pictures/RI3.png",
				"/pictures/RI4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
