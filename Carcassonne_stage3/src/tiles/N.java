package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.Road;

public class N extends Tile {
	public N(){
		Field f1 = new Field();
		Field f2 = new Field();
		Road r = new Road(this,2);
		City c = new City(this,2);
		Feature[] top = new Feature[]{c,c,c};
		Feature[] right = new Feature[]{c,c,c};
		Feature[] left = new Feature[]{f1,r,f2};
		Feature[] bottom = new Feature[]{f2,r,f1};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.25,0.15,c};
		Object[] a2 = new Object[]{0.5,0.5,f1};
		Object[] a3 = new Object[]{0.35,0.65,r};
		Object[] a4 = new Object[]{0.15,0.85,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/N1.png",
				"/pictures/N2.png",
				"/pictures/N3.png",
				"/pictures/N4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
