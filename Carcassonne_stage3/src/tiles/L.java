package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.Road;

public class L extends Tile {
	public L(){
		Field f1 = new Field();
		Field f2 = new Field();
		City c = new City(this,1);
		Road r = new Road(this,2);
		Feature[] left = new Feature[]{f1,r,f2};
		Feature[] bottom = new Feature[]{f2,r,f1};
		Feature[] right = new Feature[]{f1,f1,f1};
		Feature[] top = new Feature[]{c,c,c};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.1,0.9,f1};
		Object[] a2 = new Object[]{0.4,0.6,r};
		Object[] a3 = new Object[]{0.75,0.5,f2};
		Object[] a4 = new Object[]{0.5,0.15,c};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/L1.png",
				"/pictures/L2.png",
				"/pictures/L3.png",
				"/pictures/L4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
