package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.River;

public class RG extends Tile {
	
	public RG() {
		
		City c1 = new City(this,1);
		City c2 = new City(this,1);
		Field f1 = new Field();
		Field f2 = new Field();
		River ri = new River();
		
		Feature[] top = new Feature[]{f1,ri,f2};
		Feature[] left = new Feature[]{c1,c1,c1};
		Feature[] bottom = new Feature[]{f1,ri,f2};
		Feature[] right = new Feature[]{c2,c2,c2};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.1,0.5,c1};
		Object[] a2 = new Object[]{0.3,0.2,f1};
		Object[] a3 = new Object[]{0.8,0.5,c2};
		Object[] a4 = new Object[]{0.6,0.2,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RG1.png",
				"/pictures/RG2.png",
				"/pictures/RG3.png",
				"/pictures/RG4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
