package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;

public class E extends Tile {
	public E(){
		Field f = new Field();
		City c = new City(this,2);
		Feature[] top = new Feature[]{c,c,c};
		Feature[] right = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{f,f,f};
		Feature[] left = new Feature[]{f,f,f};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.25,0.15,c};
		Object[] a2 = new Object[]{0.25,0.75,f};
		Object[][] coordinates = new Object[][]{a1,a2};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/E1.png",
				"/pictures/E2.png",
				"/pictures/E3.png",
				"/pictures/E4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
