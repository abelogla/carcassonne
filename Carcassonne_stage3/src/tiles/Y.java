package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.Road;

public class Y extends Tile {
	public Y(){
		Field f1 = new Field();
		Field f2 = new Field();
		Field f3 = new Field();
		Road r1 = new Road(this,1);
		Road r2 = new Road(this,1);
		Road r3 = new Road(this,1);
		Feature[] top = new Feature[]{f1,f1,f1};
		Feature[] left = new Feature[]{f1,r1,f2};
		Feature[] bottom = new Feature[]{f2,r2,f3};
		Feature[] right = new Feature[]{f3,r3,f1};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.25,f1};
		Object[] a2 = new Object[]{0.15,0.85,f2};
		Object[] a3 = new Object[]{0.85,0.85,f3};
		Object[] a4 = new Object[]{0.25,0.5,r1};
		Object[] a5 = new Object[]{0.45,0.75,r2};
		Object[] a6 = new Object[]{0.75,0.5,r3};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4,a5,a6};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/Y1.png",
				"/pictures/Y2.png",
				"/pictures/Y3.png",
				"/pictures/Y4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
