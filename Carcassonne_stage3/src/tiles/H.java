package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;

public class H extends Tile {
	public H(){
		City c = new City(this,4);
		Feature[] top = new Feature[]{c,c,c};
		Feature[] left = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{c,c,c};
		Feature[] right = new Feature[]{c,c,c};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.5,c};
		Object[][] coordinates = new Object[][]{a1};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/H1.png",
				"/pictures/H2.png",
				"/pictures/H3.png",
				"/pictures/H4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
