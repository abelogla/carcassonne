package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;

public class G extends Tile {
	public G(){
		Field f1 = new Field();
		Field f2 = new Field();
		City c = new City(this,2);
		Feature[] top = new Feature[]{f1,f1,f1};
		Feature[] left = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{f2,f2,f2};
		Feature[] right = new Feature[]{c,c,c};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.2,f1};
		Object[] a2 = new Object[]{0.5,0.5,c};
		Object[] a3 = new Object[]{0.65,0.8,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/G1.png",
				"/pictures/G2.png",
				"/pictures/G3.png",
				"/pictures/G4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
