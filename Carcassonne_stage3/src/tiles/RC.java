package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.River;

public class RC extends Tile {
	
	public RC() {
		
		River ri = new River();
		Field f1 = new Field();
		Field f2 = new Field();
		
		Feature[] top = new Feature[]{f1,ri,f2};
		Feature[] left = new Feature[]{f1,f1,f1};
		Feature[] bottom = new Feature[]{f1,f1,f1};
		Feature[] right = new Feature[]{f2,ri,f1};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.3,0.5,f1};
		Object[] a2 = new Object[]{0.8,0.25,f2};
		Object[][] coordinates = new Object[][]{a1,a2};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RC1.png",
				"/pictures/RC2.png",
				"/pictures/RC3.png",
				"/pictures/RC4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
