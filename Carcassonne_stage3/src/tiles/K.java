package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.Road;

public class K extends Tile {
	public K(){
		Field f1 = new Field();
		Field f2 = new Field();
		City c = new City(this,1);
		Road r = new Road(this,2);
		Feature[] right = new Feature[]{f1,r,f2};
		Feature[] bottom = new Feature[]{f2,f2,f2};
		Feature[] left = new Feature[]{f2,r,f1};
		Feature[] top = new Feature[]{c,c,c};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.75,f1};
		Object[] a2 = new Object[]{0.5,0.5,r};
		Object[] a3 = new Object[]{0.05,0.35,f2};
		Object[] a4 = new Object[]{0.5,0.2,c};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/K1.png",
				"/pictures/K2.png",
				"/pictures/K3.png",
				"/pictures/K4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
