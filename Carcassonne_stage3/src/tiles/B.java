package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;

public class B extends Tile {
	public B(){
		Field f = new Field();
		City c1 = new City(this,1);
		City c2 = new City(this,1);
		Feature[] bottom = new Feature[]{f,f,f};
		Feature[] left = new Feature[]{f,f,f};
		Feature[] top = new Feature[]{c1,c1,c1};
		Feature[] right = new Feature[]{c2,c2,c2};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.25,0.75,f};
		Object[] a2 = new Object[]{0.5,0.15,c1};
		Object[] a3 = new Object[]{0.85,0.5,c2};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/B1.png",
				"/pictures/B2.png",
				"/pictures/B3.png",
				"/pictures/B4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
