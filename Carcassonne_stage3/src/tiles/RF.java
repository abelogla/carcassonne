package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.River;
import features.Road;

public class RF extends Tile {
	
	public RF() {
		Road r = new Road(this,2);
		River ri = new River();
		Field f1 = new Field();
		Field f2 = new Field();
		Field f3 = new Field();
		Field f4 = new Field();
		
		Feature[] top = new Feature[]{f1,r,f2};
		Feature[] left = new Feature[]{f1,ri,f3};
		Feature[] bottom = new Feature[]{f3,r,f4};
		Feature[] right = new Feature[]{f2,ri,f4};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.2,0.2,f1};
		Object[] a2 = new Object[]{0.45,0.45,r};
		Object[] a3 = new Object[]{0.2,0.8,f3};
		Object[] a4 = new Object[]{0.8,0.2,f2};
		Object[] a5 = new Object[]{0.8,0.8,f4};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4,a5};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RF1.png",
				"/pictures/RF2.png",
				"/pictures/RF3.png",
				"/pictures/RF4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
