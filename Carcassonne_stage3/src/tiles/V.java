package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.Monastery;
import features.Road;

public class V extends Tile {
	
	public V(){
		Monastery m = new Monastery();
		Field f = new Field();
		Road r = new Road(this,1);
		Feature[] top = new Feature[]{f,f,f};
		Feature[] left = new Feature[]{f,f,f};
		Feature[] bottom = new Feature[]{f,r,f};
		Feature[] right = new Feature[]{f,f,f};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.1,f};
		Object[] a2 = new Object[]{0.5,0.5,m};
		Object[] a3 = new Object[]{0.45,0.8,r};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/V1.png",
				"/pictures/V2.png",
				"/pictures/V3.png",
				"/pictures/V4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
