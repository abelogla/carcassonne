package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.Monastery;


public class U extends Tile {
	public U(){
		Monastery m = new Monastery();
		Field f = new Field();
		Feature[] top = new Feature[]{f,f,f};
		Feature[] left = new Feature[]{f,f,f};
		Feature[] bottom = new Feature[]{f,f,f};
		Feature[] right = new Feature[]{f,f,f};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.8,f};
		Object[] a2 = new Object[]{0.5,0.5,m};
		Object[][] coordinates = new Object[][]{a1,a2};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/U1.png",
				"/pictures/U2.png",
				"/pictures/U3.png",
				"/pictures/U4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
