package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.City;

public class F extends Tile {
	public F(){
		Field f1 = new Field();
		Field f2 = new Field();
		City c = new City(this,2);
		Feature[] left = new Feature[]{c,c,c};
		Feature[] top = new Feature[]{f1,f1,f1};
		Feature[] right = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{f2,f2,f2};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.1,f1};
		Object[] a2 = new Object[]{0.5,0.5,c};
		Object[] a3 = new Object[]{0.5,0.85,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/F1.png",
				"/pictures/F2.png",
				"/pictures/F3.png",
				"/pictures/F4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
