package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;

public class A extends Tile {
	public A(){
		City c = new City(this,1);
		Field f = new Field();
		Feature[] left = new Feature[]{f,f,f};
		Feature[] top = new Feature[]{c,c,c};
		Feature[] right = new Feature[]{f,f,f};
		Feature[] bottom = new Feature[]{f,f,f};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.1,c};
		Object[] a2 = new Object[]{0.5,0.5,f};
		Object[][] coordinates = new Object[][]{a1,a2};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/A1.png",
				"/pictures/A2.png",
				"/pictures/A3.png",
				"/pictures/A4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
