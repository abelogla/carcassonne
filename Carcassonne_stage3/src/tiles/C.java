package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;

public class C extends Tile {
	public C(){
		Field f = new Field();
		City c1 = new City(this,1);
		City c2 = new City(this,1);
		Feature[] left = new Feature[]{f,f,f};
		Feature[] top = new Feature[]{c1,c1,c1};
		Feature[] right = new Feature[]{f,f,f};
		Feature[] bottom = new Feature[]{c2,c2,c2};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.15,c1};
		Object[] a2 = new Object[]{0.5,0.5,f};
		Object[] a3 = new Object[]{0.5,0.85,c2};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/C1.png",
				"/pictures/C2.png",
				"/pictures/C3.png",
				"/pictures/C4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
