package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.Road;

public class Q extends Tile {
	public Q(){
		Field f1 = new Field();
		Field f2 = new Field();
		City c = new City(this,3);
		Road r = new Road(this,1);
		Feature[] top = new Feature[]{c,c,c};
		Feature[] left = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{f1,r,f2};
		Feature[] right = new Feature[]{c,c,c};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.5,c};
		Object[] a2 = new Object[]{0.45,0.85,r};
		Object[] a3 = new Object[]{0.65,0.85,f1};
		Object[] a4 = new Object[]{0.35,0.85,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/Q1.png",
				"/pictures/Q2.png",
				"/pictures/Q3.png",
				"/pictures/Q4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
