package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.Monastery;
import features.River;
import features.Road;

public class RD extends Tile {
	
	public RD() {
		
		Monastery m = new Monastery();
		River ri = new River();
		Road r = new Road(this,1);
		Field f1 = new Field();
		Field f2 = new Field();
		Field f3 = new Field();
		
		Feature[] top = new Feature[]{f1,f1,f1};
		Feature[] left = new Feature[]{f1,ri,f2};
		Feature[] bottom = new Feature[]{f2,r,f3};
		Feature[] right = new Feature[]{f1,ri,f3};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.7,0.1,f1};
		Object[] a2 = new Object[]{0.5,0.4, m};
		Object[] a3 = new Object[]{0.2,0.8,f2};
		Object[] a4 = new Object[]{0.8,0.8,f3};
		Object[] a5 = new Object[]{0.45, 0.9,r};
		Object[][] coordinates = new Object[][]{a1,a2,a3,a4,a5};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RD1.png",
				"/pictures/RD2.png",
				"/pictures/RD3.png",
				"/pictures/RD4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
