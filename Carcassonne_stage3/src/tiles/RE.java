package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.City;
import features.Field;
import features.River;

public class RE extends Tile {
	
	public RE() {
		City c = new City(this,2);
		River ri = new River();
		Field f1 = new Field();
		Field f2 = new Field();
		
		Feature[] top = new Feature[]{f1,ri,f2};
		Feature[] left = new Feature[]{c,c,c};
		Feature[] bottom = new Feature[]{c,c,c};
		Feature[] right = new Feature[]{f2,ri,f1};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.3,0.1,f1};
		Object[] a2 = new Object[]{0.8,0.2,f2};
		Object[] a3 = new Object[]{0.4,0.5,c};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RE1.png",
				"/pictures/RE2.png",
				"/pictures/RE3.png",
				"/pictures/RE4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
