package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.River;

public class RA extends Tile {
	
	public RA() {
		River ri = new River();
		Field f = new Field();
		
		Feature[] top = new Feature[]{f,f,f};
		Feature[] left = new Feature[]{f,f,f};
		Feature[] bottom = new Feature[]{f,ri,f};
		Feature[] right = new Feature[]{f,f,f};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.8,0.3,f};
		Object[][] coordinates = new Object[][]{a1};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RA1.png",
				"/pictures/RA2.png",
				"/pictures/RA3.png",
				"/pictures/RA4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // Test
}
