package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.River;

public class RB extends Tile {
	
	public RB() {
		
		River ri = new River();
		Field f1 = new Field();
		Field f2 = new Field();
		
		Feature[] top = new Feature[]{f1,ri,f2};
		Feature[] left = new Feature[]{f1,f1,f1};
		Feature[] bottom = new Feature[]{f1,ri,f2};
		Feature[] right = new Feature[]{f2,f2,f2};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.1,0.2,f1};
		Object[] a2 = new Object[]{0.8,0.8,f2};
		Object[][] coordinates = new Object[][]{a1,a2};
		super.setCoordinates(coordinates);
		String[] picturePaths = new String[]{
				"/pictures/RB1.png",
				"/pictures/RB2.png",
				"/pictures/RB3.png",
				"/pictures/RB4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
