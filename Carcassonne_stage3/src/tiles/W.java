package tiles;

import java.io.IOException;

import carcassonne.Feature;
import carcassonne.Tile;
import features.Field;
import features.Road;

public class W extends Tile {
	public W(){
		Field f1 = new Field();
		Field f2 = new Field();
		Road r = new Road(this,2);
		Feature[] right = new Feature[]{f1,r,f2};
		Feature[] bottom = new Feature[]{f2,f2,f2};
		Feature[] left = new Feature[]{f2,r,f1};
		Feature[] top = new Feature[]{f1,f1,f1};
		Feature[][] edges = new Feature[][]{top,right,bottom,left};
		super.initializeEdges(edges);
		
		Object[] a1 = new Object[]{0.5,0.55,r};
		Object[] a2 = new Object[]{0.5,0.75,f1};
		Object[] a3 = new Object[]{0.5,0.25,f2};
		Object[][] coordinates = new Object[][]{a1,a2,a3};
		super.setCoordinates(coordinates);
		
		String[] picturePaths = new String[]{
				"/pictures/W1.png",
				"/pictures/W2.png",
				"/pictures/W3.png",
				"/pictures/W4.png"};
		try {
			super.setImage(picturePaths);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
