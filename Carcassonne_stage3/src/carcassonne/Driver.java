package carcassonne;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Driver {

	public static void main(String[] args) {
//		try {
//		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
//		    	System.out.println(info.getName());
//		        if ("Metal".equals(info.getName())) {
//		            UIManager.setLookAndFeel(info.getClassName());
//		            break;
//		        }
//		    }
//		} catch (Exception e) {
//
//		}
		if(args.length < 1) System.out.println("Give me some arguments!");
		else if(args.length == 1) {
			Model m = new Model(args[0]);
			new View(m);
		}
		else {
			for (int i=0; i<args.length; i++) {
				System.out.println("args["+i+"] is \""+args[i]+"\".");
			}
			Model m = new Model(args);
			new View(m);
		}
	}
}
