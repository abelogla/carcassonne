package carcassonne;

public class Player {
	private String _name, _follower, _color;
	private int _followers,_score;

	public Player(String name,String color, int score){
		switch(color){
		case "RED":
			_follower = "/pictures/REDman.png";
			break;
		case "BLUE":
			_follower = "/pictures/BLUEman.png";
			break;
		case "YELLOW":
			_follower = "/pictures/YELLOWman.png";
			break;
		case "PINK":
			_follower = "/pictures/PINKman.png";
			break;
		case "WHITE":
			_follower = "/pictures/WHITEman.png";
			break;
		}
		_color=color;
		_score=0;
		_followers=8;
		_name=name;
		_score = score;
	}
	/**
	 * A getter method for the NAME of the Player.
	 * @return the name of the player as a String.
	 */
	public String getName(){
		return _name;
	}
	
	public String getColor(){
		return _color;
	}
	
	/**
	 * This method is called by Model to set the new number of followers that a player has
	 * once one has been placed.
	 * @param i is the new number of followers for a Player
	 */
	public void setFollowers(int i){
		_followers = i;
	}
	/**
	 * A getter method for the FILE PATH of the associated PNG picture representing the FOLLOWER of the Player.
	 * @return the file path of the image as a String.
	 */
	public String getFollowerImage(){
		return _follower;
	}
	/**
	 * A getter method for the SCORE of the Player.
	 * @return the score of the Player as an int.
	 */
	public int getScore(){
		return _score;
	}
	
	public void setScore(int score){
		_score += score;
	}
	/**
	 * A getter method for the number of followers of the Player.
	 * @return the number of followers of the Player as an int.
	 */
	public int getFollowers(){
		return _followers;
	}
}
