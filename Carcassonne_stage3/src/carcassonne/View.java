package carcassonne;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.MouseInputAdapter;

import tiles.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class View {
	private int[] _currentSize;
	private int _w,_h, _scrollX, _scrollY;
	private Model _model;
	private JFrame _frame;
	private JPanel _leftPanel,_rightPanel,_controlsPanel,_infoPanel,_chooseFollowersPanel,_playerInfoPanel;
	private JScrollPane scroll;
	private JButton[][] _board;
	private JLabel _errorMessage,_followers,_playerScore,_bottomLabel,_playerName;
	private JButton _rotateButton,_skipButton,_saveGame;
	private class MyListener extends MouseInputAdapter {
		@Override
		public void mouseMoved(MouseEvent e) {
			//System.out.println("X: "+(int)_leftPanel.getMousePosition().getX()+"Y: "+(int)_leftPanel.getMousePosition().getY());
			if((int)scroll.getMousePosition().getX() > 400) scroll.getHorizontalScrollBar().setValue(scroll.getHorizontalScrollBar().getValue()+3);
			if((int)scroll.getMousePosition().getX() > 450) scroll.getHorizontalScrollBar().setValue(scroll.getHorizontalScrollBar().getValue()+6);
			if((int)scroll.getMousePosition().getY() > 400) scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getValue()+3);
			if((int)scroll.getMousePosition().getY() > 450) scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getValue()+6);
			if((int)scroll.getMousePosition().getX() < 160) scroll.getHorizontalScrollBar().setValue(scroll.getHorizontalScrollBar().getValue()-3);
			if((int)scroll.getMousePosition().getX() < 110) scroll.getHorizontalScrollBar().setValue(scroll.getHorizontalScrollBar().getValue()-6);
			if((int)scroll.getMousePosition().getY() < 160) scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getValue()-3);
			if((int)scroll.getMousePosition().getY() < 110) scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getValue()-6);

			
		}
	}
	MyListener myl = new MyListener();
	/**
	 * The constructor sets up a 3x3 board on the left, and a controls panel on the right. From top down:
	 * "Player Name", "Followers and Score", "Error Message", "Rotate and Skip", "Current Tile."
	 * @param m of type Model associates the View with the game's model
	 */
	public View(Model m) {
		Tile d = new RA();
		_w = d.getImage().getIconWidth();
		_h = d.getImage().getIconHeight();
		// create components
		_frame = new JFrame("Carcassonne: Stage 3");

		_model = m;
		_model.addObserver(this);
		
		_leftPanel = new JPanel();
		_rightPanel = new JPanel();
		_rightPanel.setOpaque(true);
		_rightPanel.setBackground(new Color(216,242,225));
		_frame.setLayout(new GridLayout(1,2));
		scroll = new JScrollPane(_leftPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
        		scroll.getVerticalScrollBar().setValue(_scrollY);
        		scroll.getHorizontalScrollBar().setValue(_scrollX);
            }
        });
		_frame.add(scroll);
		_frame.add(_rightPanel);
		_chooseFollowersPanel = new JPanel();
		
		_rightPanel.setLayout(new GridLayout(5,1));
			_playerName = new JLabel("",SwingConstants.CENTER);
			_playerName.setOpaque(true);
			_playerName.setBackground(new Color(216,242,225));
				_rightPanel.add(_playerName);
			_infoPanel = new JPanel();
				_infoPanel.setLayout(new GridLayout(2,1));
				_playerInfoPanel = new JPanel();
				_playerInfoPanel.setOpaque(true);
				_playerInfoPanel.setBackground(new Color(216,242,225));
					_followers = new JLabel("",SwingConstants.CENTER);
					_playerScore = new JLabel("",SwingConstants.CENTER);
					_playerInfoPanel.add(_followers);
					_playerInfoPanel.add(_playerScore);
					_playerInfoPanel.setLayout(new GridLayout(1,2));
				_errorMessage = new JLabel("Scoring works for roads and cities only",SwingConstants.CENTER);
				_errorMessage.setForeground(Color.RED);
				_errorMessage.setOpaque(true);
				_errorMessage.setBackground(new Color(216,242,225));
				_infoPanel.add(_playerInfoPanel);
				_infoPanel.add(_errorMessage);
				_rightPanel.add(_infoPanel);
				_saveGame = new JButton("Save game");
				_saveGame.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
					_model.saveGame();
					}});
				_rightPanel.add(_saveGame);
			_controlsPanel = new JPanel();
			_controlsPanel.setOpaque(true);
			_controlsPanel.setBackground(new Color(216,242,225));
				_controlsPanel.setLayout(new GridLayout(1,2));
				_bottomLabel = new JLabel("",null,JLabel.CENTER);
				_bottomLabel.setOpaque(true);
				_bottomLabel.setBackground(new Color(216,242,225));
				_rotateButton = new JButton("Rotate");
				_rotateButton.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						_model.getCurrentTile().rotate();
						_bottomLabel.setIcon(_model.getCurrentTile().getImage());
					}});
				_controlsPanel.add(_rotateButton);
				_skipButton = new JButton("Skip");
				_skipButton.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						_model.skip();
					}});
				_controlsPanel.add(_skipButton);
				_rightPanel.add(_controlsPanel);
			_rightPanel.add(_bottomLabel);	
		_currentSize = _model.getBoardSize();
		_frame.setVisible(true);
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.update();
	}
	/**
	 * This method obtains info on the current board size, current board contents, and current tile
	 * and updates the UI accordingly.
	 */
	public void update() {
		_currentSize = _model.getBoardSize();
		_leftPanel.removeAll();
		_leftPanel.setLayout(new GridLayout(_currentSize[0],_currentSize[1]));
		_rightPanel.add(_controlsPanel);
		_rightPanel.add(_bottomLabel);
		_bottomLabel.setIcon(_model.getCurrentTile().getImage());
		_chooseFollowersPanel.removeAll();
		_saveGame.setVisible(true);
		
		_playerName.setText("Current Player: "+_model.getPlayer().getName());
		_followers.setText("Followers: "+_model.getPlayer().getFollowers());
		_playerScore.setText("Score: "+_model.getPlayer().getScore());
		int nx=7, ny=7;
		if(_currentSize[0] <= 7) ny=_currentSize[0];
		if(_currentSize[1] <= 7) nx=_currentSize[1];
		_frame.setSize(nx*2*_w, ny*(4+_h));
/*		if (_currentSize[0]<5){_frame.setSize(_currentSize[1]*2*(_w),5*(2+_h));}
		else if (_currentSize[0]>=5){_frame.setSize(_currentSize[1]*2*(_w),_currentSize[0]*(4+_h));}*/
		_board = new JButton[_currentSize[0]][_currentSize[1]];
		for (int r=0;r<_currentSize[0];r++){
			for (int c=0;c<_currentSize[1];c++){
				_board[r][c] = new JButton();
				_board[r][c].setBorder(new LineBorder(new Color(230,230,230), 1));
				_board[r][c].setOpaque(true);
				_board[r][c].setIcon(new ImageIcon(this.getClass().getResource("/pictures/tex.png")));
				_board[r][c].addActionListener(new PlaceTile(_model,r,c));
				_board[r][c].addMouseMotionListener(myl);
				_leftPanel.add(_board[r][c]);
				if (_model.getTile(r,c)!=null){_board[r][c].setIcon(_model.getTile(r,c).getImage());}
				_board[r][c].setSize(_w,_h);
			}
		}
	}
	/**
	 * This method is called to alert the user in cases of illegal moves, or to indicate what action 
	 * they need to take.
	 * @param string
	 */
	public void alert(String string) {
		_errorMessage.setText(string);
	}
	/**
	 * This method is called when a user selects a valid position to place the current tile. It prompts the
	 * player to select a position to place their follower and subsequently tells the Model to 
	 * finalize the move and update the board.
	 * @param tile of type Tile supplies the "current tile"
	 * @param followerImages of Type ImageIcon[] supplies an array of images of the possible positions to place a follower
	 */
	public void chooseFollower(Tile tile,ImageIcon[] followerImages, ArrayList<Integer> featureIndex) {
		_scrollY = scroll.getVerticalScrollBar().getValue();
		_scrollX = scroll.getHorizontalScrollBar().getValue();
		_leftPanel.removeAll();
		_leftPanel.add(_chooseFollowersPanel);
		_leftPanel.setLayout(new GridLayout(1,1));
		_rightPanel.remove(_bottomLabel);
		_rightPanel.remove(_controlsPanel);
		_saveGame.setVisible(false);
		_chooseFollowersPanel.setLayout(new GridLayout(followerImages.length,1));
		alert(" Choose where to (or to not) place your follower ");
		for (int i=0;i<followerImages.length;i++){
			JButton b = new JButton(followerImages[i]);
			if (i==0){b.addActionListener(new AddFollower(_model,tile,followerImages[i],null));}
			else {b.addActionListener(new AddFollower(_model,tile,followerImages[i],tile.getCoordinates()[featureIndex.get(i-1)][2]));}
			b.setSize(_w,_h);
			_chooseFollowersPanel.add(b);
		}
		_frame.pack();
	}
}


