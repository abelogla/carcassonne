package carcassonne;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlaceTile implements ActionListener {
	/**
	 * Holds the ROW on the board that the button is in.
	 */
	private int _row;
	/**
	 * Holds the COLUMN on the board that the button is in.
	 */
	private int _column;
	/**
	 * Holds the reference to the game's Model.
	 */
	private Model _m;
	/**
	 * This constructor accepts parameters Model, Row, and Column that describe
	 * the button position.
	 * @param m of type Model is the reference to the game's Model, passed by the View
	 * @param r of type int is the button's row INDEX, passed by the View
	 * @param c of type int is the button's column INDEX, passed by the View
	 */
	public PlaceTile(Model m,int r,int c){
		_row = r;
		_column = c;
		_m = m;
	}
	/**
	 * This method tells the model to begin the move-making process.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		_m.makeMove(_row,_column);
	}
}
