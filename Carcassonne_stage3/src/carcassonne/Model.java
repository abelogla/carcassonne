package carcassonne;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import tiles.*;
import bag.TileBag;
import bag.TileGenerator;

public class Model {
	
	private Tile[][] _board,_newboard;
	private Tile _currentTile;
	private TileBag _bag;
	private View _view;
	private Player[] _players;
	private int _player,_a,_b;
	private int[] _boardSize;
	private boolean _needResize,_touching;
	private ImageIcon[] _followerImages;
	/**
	 * This constructor accepts an array of strings from the command line args, 
	 * which is stored as an array of player names. Then, all instance variables to be used are initialized.
	 * @param names is an array containing String objects representing player names
	 */
	public Model(String[] names) {
		_boardSize = new int[]{7,7};
		_board = new Tile[_boardSize[0]][_boardSize[1]];
		_bag = new TileBag();
		_board[_boardSize[0]/2][_boardSize[1]/2] = new RA();
		_currentTile=_bag.getTile();
		String[] colors = new String[] {
				"RED",
				"BLUE",
				"YELLOW",
				"PINK",
				"WHITE"
		};
		_players = new Player[names.length];
		for (int i=0;i<names.length;i++){
			_players[i]=new Player(names[i],colors[i],0);
		}
		_touching = false;
		_needResize = false;
		_newboard=_board;
	}

	public Model(String path) {
		try {
			Scanner file = new Scanner(new FileReader(path));
			System.out.println("Open file: "+path);
			//	**********************
			//	***Creating players***
			//	**********************
			String[] players, player;
			players = file.nextLine().split("(\\]\\,\\[)|\\[|\\]");
			players = Arrays.stream(players)
					.filter(o -> (o != null && o.length() > 0))
					.toArray(String[]::new);
			_players = new Player[players.length];
			for (int i=0;i<players.length;i++){
				player = players[i].split(",");
				System.out.println("Creating player: "+player[0]);
				_players[i]=new Player(player[0],player[1],Integer.parseInt(player[2]));
			}
			//	*******************************************************************
			//	***Creating tiles and put them on board connecting to each other***
			//	*******************************************************************
			String[] tile;
			String[][] tiles;
			int m_counter=0, row=0, boardsizeX=0, boardsizeY=0;
			tile = file.nextLine().split("(\\]\\,)+|\\,(?!\\d+\\])|(\\)\\[)|\\(|(\\)\\,)+|\\]|\\)");
			for(String str:tile)
				if(str.contains(",")) m_counter++;
			tiles = new String[(tile.length-m_counter)/3][4];
			for(int i=0; i<tile.length; i=i+3) {
				if(tile[i].contains(",")) {
					tiles[row-1][3] = tile[i];
					i = i-2;
				}
				else {
					tiles[row][0] = tile[i];
					tiles[row][1] = tile[i+1];
					tiles[row][2] = tile[i+2];
					row++;
				}
			}
			// Defining the board size
			for(int i=0; i<tiles.length;i++) {
				boardsizeX = (Integer.parseInt(tiles[i][1]) > boardsizeX) ? 
						Integer.parseInt(tiles[i][1]) : boardsizeX;
				boardsizeY = (Integer.parseInt(tiles[i][2]) > boardsizeY) ? 
						Integer.parseInt(tiles[i][2]) : boardsizeY;
			}
			//if(boardsizeX<5 || boardsizeY<5) { boardsizeX=5; boardsizeY=5; }
			_boardSize = new int[]{boardsizeX+2,boardsizeY+2};
			_board = new Tile[_boardSize[0]][_boardSize[1]];
			
			// Generating tiles
			TileGenerator tg = new TileGenerator();
			System.out.print("Placing " + tiles.length + " tiles on the board: ");
			for(int i=0; i<tiles.length; i++) {
				Tile newTile = tg.generateTile(tiles[i][0]);
				_currentTile = newTile;
				System.out.print(tiles[i][0]+" ");
				// If follower presents then create it and put on the tile
				if(tiles[i][3] != null) {
					String[] followerInfo = tiles[i][3].split(",");
					int playerIndex = 0, featureIndex = 0, followerX, followerY,
							followerPosition = Integer.parseInt(followerInfo[1]);
					Feature feature;
					// Searching an owner of the follower
					for(int j=0;j<_players.length;j++)
						if(_players[j].getName().equals(followerInfo[0])) playerIndex = j;
					// Searching an index of Feature-with-follower in _coordinates
					// non Monastery case
					if(followerPosition < 12) {
						followerX = followerPosition/3;
						followerY = followerPosition%3;
						feature = newTile.getEdge(followerX)[followerY];
						for(int j=0;j<newTile.getCoordinates().length;j++)
							if(newTile.getEdge(followerX)[followerY] == newTile.getCoordinates()[j][2]) featureIndex = j;
						// Placing the follower in corresponding Feature
						newTile.setFollower(feature,new Follower(newTile,feature,_players[playerIndex]));
						System.out.print("<-(follower at: "+followerPosition+") ");
					}
					// follower in Monastery
					else {
						System.out.print("(follower in Monastery) ");
						for(int j=0;j<newTile.getCoordinates().length;j++) {
							if (newTile.getCoordinates()[j][2].getClass().getSimpleName().equals("Monastery")) {
								feature = (Feature)newTile.getCoordinates()[j][2];
								// Placing the follower in Monastery
								newTile.setFollower(feature,new Follower(newTile,feature,_players[playerIndex]));
								featureIndex = j;
							}
						}
					}
					_players[playerIndex].setFollowers(_players[playerIndex].getFollowers()-1);
					// Creating new tile image with follower
					BufferedImage baseImage = (BufferedImage) newTile.getImage().getImage();
					BufferedImage followerImage = ImageIO.read(this.getClass().getResourceAsStream(_players[playerIndex].getFollowerImage()));
					int w = Math.max(baseImage.getWidth(),baseImage.getWidth());
					int h = Math.max(baseImage.getHeight(),baseImage.getHeight());
					BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
					Graphics g = combined.getGraphics();
					g.drawImage(baseImage, 0, 0, null);
					g.drawImage(followerImage,(int)(w*(double)newTile.getCoordinates()[featureIndex][0]),
							(int)(h*(double)newTile.getCoordinates()[featureIndex][1]), null);
					newTile.setCurrentImage(new ImageIcon(combined));
					g.dispose();
				}
				System.out.println();
				connect(Integer.parseInt(tiles[i][1]),Integer.parseInt(tiles[i][2]));
				_board[Integer.parseInt(tiles[i][1])][Integer.parseInt(tiles[i][2])] = _currentTile;
			}
			//	*****************************************************************
			//	***Filling up the bag with remaining tiles and finishing setup***
			//	*****************************************************************
			_bag = new TileBag(file.nextLine().split(","));
			file.close();
			_currentTile=_bag.getTile();
			_touching = false;
			_needResize = false;
			_newboard=_board;
			_player =0;
		}
		catch(IOException e) {
			System.out.println("Cannot read the file or it doesn't exist");
			System.exit(1);
		}
		catch(Exception e) {
			System.out.println("File is corrupted or unsupported format. Give me another one ;)");
			System.exit(1);
		}
	}

	
	/**
	 * This method accepts coordinates a (row) and b (column) from the view, first checks to see 
	 * if the move is legal, and then prompts the player to choose a follower (and position).
	 * @param a is an integer representing the intended Tile row
	 * @param bis an integer representing the intended Tile column
	 */
	public void makeMove(int a, int b){
		boolean river = (_currentTile.getEdge(0)[1].getClass().getSimpleName().equals("River"));
		if (checkMove(a,b)==true && _touching == true){
			//*********************************
			//**** Connecting Tiles  **********
			//*********************************
			if(river && _board[a-1][b]!=null || !river) {
				connect(a, b);

				_a=a;
				_b=b;
				_board[a][b]=_currentTile;
				System.out.println("Current board size: "+_boardSize[0]+" "+_boardSize[1]);
				System.out.println("Placing tile at: "+_a+" "+_b);
				if(_players[_player].getFollowers() !=0) chooseFollower(_currentTile);
				else finishMove(null);
			}
			else _view.alert("River U-turn is not allowed!");
		}
		else if (checkMove(a,b)==false){
			_view.alert("Illegal move");
		}
		else if (_touching == false) {
			_view.alert("Choose contiguous board tile");
		}
	}
	/**
	 * This method is called by the ActionListener on the chooseFollower buttons after the player
	 * has selected a Follower and Feature to place it on. It then associates the follower with the
	 * current tile and then draws a new tile, advances the move to the next player, and resizes the
	 * board if needed. View update follows.
	 * @param feature of type Object inputs the Feature that the player has chosen to place his/her
	 * follower on
	 */
	public void finishMove(Feature feature) {
		if (feature != null){
			_currentTile.setFollower((Feature)feature, new Follower(_currentTile,(Feature)feature,_players[_player]));
			_players[_player].setFollowers(_players[_player].getFollowers()-1);
			_currentTile.updateFollowersOnFeatures();
			System.out.println(">>> Placed follower on tile "+_currentTile.tileName()+" at position "+_currentTile.getFollowerPosition());
		}
		if (_needResize == true){
			resizeBoard(_a,_b);
		}
		_view.alert("");
		score();
		_player=(_player+1)%_players.length;
		_currentTile=_bag.getTile();
		_touching = false;
		_view.update();
	}
	/**
	 * This method checks to see whether a move placing a Tile to coordinate (row,column) 
	 * is legal and returns true if it is.
	 * @param row is an integer variable that represents the "row" in which the Tile will be placed
	 * @param column is an integer variable that represents the "column" in which the Tile will be placed
	 * @return true if the move is legal, false if it is not.
	 */
	public boolean checkMove(int row, int column) {
		if (_board[row][column]!=null){
			return false;
		}
		int x=0,y=0;
		for (int i=0;i<4;i++){
			if (i==0) {x=row-1;y=column;}		// top
			else if (i==1) {x=row;y=column+1;}	// right
			else if (i==2) {x=row+1;y=column;}	// bottom
			else if (i==3) {x=row;y=column-1;}	// left
			if (x<0||x>=_boardSize[0]||y<0||y>=_boardSize[1]){
				_needResize = true;
			}
			else if (_board[x][y]==null){;}
			else if ((_currentTile.tileName().length() > 2) && !_board[x][y].getEdge((i+2)%4)[1].getClass().getSimpleName().equals("River")) {
				return false;
			}
			else if (!((_currentTile.getEdge(i)[1]).getClass()).equals((_board[x][y].getEdge((i+2)%4)[1]).getClass())){
				return false;
			}
			else {_touching = true;}
		}
		
		return true;
	}

	public void connect(int row, int column) {
		int x=0,y=0;
		for (int i=0;i<4;i++){
			if (i==0) {x=row-1;y=column;}	// top
			if (i==1) {x=row;y=column+1;}	// right
			if (i==2) {x=row+1;y=column;}	// bottom
			if (i==3) {x=row;y=column-1;}	// left
			if (!(x<0||x>=_boardSize[0]||y<0||y>=_boardSize[1])){	// **Accessing tiles within the board only**
				if (_board[x][y]!=null) { 
					if (_currentTile.getEdge(i)[1].getClass().getSimpleName().equals("City")||_currentTile.getEdge(i)[1].getClass().getSimpleName().equals("Road")){
						Tile touchingTile = _board[x][y];
						_currentTile.getEdge(i)[1].setSameFeatures(touchingTile.getEdge((i+2)%4)[1].getSameFeatures());
						_currentTile.getEdge(i)[1].updateNeighbors();
						int newOpennings=0;
						newOpennings = (touchingTile.getEdge((i+2)%4)[1].getOpennings()-1)+(_currentTile.getEdge(i)[1].getOpennings()-1);
						_currentTile.getEdge(i)[1].updateOpennings(touchingTile,newOpennings);
						if(_currentTile.getEdge(i)[1].getOpennings() <= 0) System.out.println("*** Completed "+_currentTile.getEdge(i)[1].getClass().getSimpleName()+" ***");
						_currentTile.getEdge(i)[1].setFollowers(touchingTile.getEdge((i+2)%4)[1].getFollowers());
						_currentTile.getEdge(i)[1].addTiles(touchingTile.getEdge((i+2)%4)[1].getTiles());
					}
				}
			}
		}
	}


	/**
	 * This method is called when a Tile is going to be placed on one of the outer edges 
	 * of the board. It accepts two integers, x and y (row,column), from makeMove() representing 
	 * the row and column the Tile will be added to and resizes the board accordingly.
	 * @param x is an integer variable that represents the "row" in which the Tile will be placed
	 * @param y is an integer variable that represents the "column" in which the Tile will be placed
	 */
	private void resizeBoard(int x,int y) {
		if (x==0){
			_boardSize[0] = _boardSize[0] + 1;
			_newboard = new Tile[_boardSize[0]][_boardSize[1]];
			for (int a=0;a<_boardSize[0]-1;a++){
				for (int b=0;b<_boardSize[1];b++){
						_newboard[a+1][b]=_board[a][b];
				}
			}
		}
		else if (x==_boardSize[0]-1){
			_boardSize[0] = _boardSize[0] + 1;
			_newboard = new Tile[_boardSize[0]][_boardSize[1]];
			for (int a=0;a<_boardSize[0]-1;a++){
				for (int b=0;b<_boardSize[1];b++){
						_newboard[a][b]=_board[a][b];
				}
			}
		}
		else if (y==0){
			_boardSize[1] = _boardSize[1] + 1;
			_newboard = new Tile[_boardSize[0]][_boardSize[1]];
			for (int a=0;a<_boardSize[0];a++){
				for (int b=0;b<_boardSize[1]-1;b++){
						_newboard[a][b+1]=_board[a][b];
				}
			}
		}
		else if(y==_boardSize[1]-1){
			_boardSize[1] = _boardSize[1] + 1;
			_newboard = new Tile[_boardSize[0]][_boardSize[1]];
			for (int a=0;a<_boardSize[0];a++){
				for (int b=0;b<_boardSize[1]-1;b++){
						_newboard[a][b]=_board[a][b];
				}
			}
		}
		_board = _newboard;
		System.out.println(">>> Resizing board to: "+_boardSize[0]+" "+_boardSize[1]);
		_view.update();
		_needResize = false;
	}
	/**
	 * This method is called if a player cannot (or does not want to) place a land Tile during 
	 * his/her turn. As its name suggests, it discards the current Tile, draws a new one, and 
	 * skips over the current player's turn, and moves onto the next.
	 */
	public void skip() {
		_view.alert(_players[_player].getName()+" skipped their turn.");
		_bag.takeTile(_currentTile);
		_currentTile=_bag.getTile();
		_player=(_player+1)%_players.length;
		_view.update();
	}
	/**
	 * This method is called by the View to connect the model and the view
	 * @param view is an instance of a View object representing the GUI the model will be connected to
	 */
	public void addObserver(View view) {
		_view = view;
	}
	/**
	 * This getter method is called by the View to obtain the current player's name
	 * @return a String object representing the name of the current player
	 */
	public String getPlayerName(){
		return _players[_player].getName();
	}
	/**
	 * This method returns the Tile object stored at the requested coordinate (i,j) (row, column) on the board.
	 * @param i is an integer representing the "row" of the requested Tile object.
	 * @param j is an integer representing the "column" of the requested Tile object.
	 * @return an object of type Tile
	 */
	public Tile getTile(int i,int j){
		return _board[i][j];
	}
	/**
	 * Called by the GUI to obtain the dimensions of the current board
	 * @return an array of type int that contains the number of rows and columns.
	 */
	public int[] getBoardSize(){
		return _boardSize;
	}
	/**
	 * This method returns the reference to the Tile that the current player is in the process of placing
	 * @return an object of type Tile
	 */
	public Tile getCurrentTile() {	
		return _currentTile;
	}
	/**
	 * This method accepts a key (player name) in the form of a String and returns the number of followers 
	 * that the player currently has as an integer.
	 * @param s is a String that represents a player's name
	 * @return an integer representing the number of followers at a player's disposal
	 */
	public Player getPlayer() {
		return _players[_player];
	}
	/**
	 * This method is called once the player has selected a valid placement for the current tile. It generates
	 * a selection of possible Follower placements based on predetermined coordinates, and displays them, prompting
	 * the player to choose.
	 * @param tile of type Tile represents the current tile for which the move is being made on.
	 */
	private void chooseFollower(Tile tile) {

		ArrayList<Integer> indexes = new ArrayList<Integer>();
		try {
			int emptyFeatures = 0;
			for(int i=0; i<tile.getCoordinates().length; i++) {
				Feature temp;
				temp = (Feature)tile.getCoordinates()[i][2];
				if(temp.getFollowers().isEmpty()) {
					indexes.add(i);
					emptyFeatures++;
				}			
			}
			_followerImages = new ImageIcon[emptyFeatures+1];
			BufferedImage base = (BufferedImage) tile.getImage().getImage();
			_followerImages[0] = new ImageIcon(base);
			for (int i=0;i<emptyFeatures;i++){
				BufferedImage temp = base;
				BufferedImage image = ImageIO.read(this.getClass().getResourceAsStream(_players[_player].getFollowerImage()));
				int w = Math.max(temp.getWidth(),temp.getWidth());
				int h = Math.max(temp.getHeight(),temp.getHeight());
				BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
				Graphics g = combined.getGraphics();
				g.drawImage(base, 0, 0, null);
				g.drawImage(image,(int)(w*(double)tile.getCoordinates()[indexes.get(i)][0]),(int)(h*(double)tile.getCoordinates()[indexes.get(i)][1]), null);
				_followerImages[i+1]=new ImageIcon(combined);
				g.dispose();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		_view.chooseFollower(tile,_followerImages, indexes);

	}

	public void saveGame() {
		String string1="", string2 = "", string3 = "",
				fileName = "SaveFile.csn";
		int k=_player;
		do {
			string1 += "["+_players[k].getName()+","+_players[k].getColor()+","+_players[k].getScore()+"]";
			k=(k+1)%_players.length;
			if(k != _player) string1 += ",";
		}
		while (k != _player);
		for(int i=0; i<_boardSize[0]; i++)
			for (int j=0; j<_boardSize[1]; j++) {
				if (_board[i][j] != null) {
					string2 += _board[i][j].tileName()+"("+i+","+j+")";
					if(_board[i][j].getFollower() != null) {
						string2 += "["+_board[i][j].getFollower().getPlayerName()+","+_board[i][j].getFollowerPosition()+"]";
					}
					if(i+1 != _boardSize[0]) string2 += ",";
				}
			}
		string2 = string2.substring(0, string2.length()-1);
		string3 = _bag.getRemainTiles()+_currentTile.tileName();
		try {
			File file = new File(fileName);
			if(!file.exists()) file.createNewFile();
			FileWriter writer = new FileWriter(file.getAbsoluteFile());
			writer.write(string1+"\n"+string2+"\n"+string3);
			writer.close();
			System.out.println("Game was saved in: "+fileName);
			JOptionPane.showMessageDialog(null, "Game was successfully saved in: "+fileName, "Message", JOptionPane.PLAIN_MESSAGE);
		}
		catch (IOException e) {System.out.println("Cannot create a file!");};
	}
	
	private void score() {
		for(int i=0; i<_currentTile.getCoordinates().length; i++) {
			Feature temp = (Feature)_currentTile.getCoordinates()[i][2];
			if(temp.getOpennings() <= 0 && temp.getFollowers().size()>0) {
				Follower f = temp.getFollowers().get(temp.getFollowers().size()-1);
				f.getPlayer().setScore(temp.returnScore());
				_view.alert(f.getPlayerName()+" scored "+temp.returnScore()+" points");
				playSound();
				f.getPlayer().setFollowers(f.getPlayer().getFollowers()+1);
				for(Tile t:temp.getTiles()) {
					if(temp.getFollowers().contains(t.getFollower())) {
						t.resetCurrentImage();
						t.setFollower(null);
					}
				}
				for(Feature ft:temp.getSameFeatures()) {
					ft.setAllFollowers(new ArrayList<Follower>());
				}
			}
		}
		_view.update();
	}
	
	private void playSound() {
		try {
			Clip clip = (Clip) AudioSystem.getClip();
	        AudioInputStream stream = AudioSystem.getAudioInputStream(
	          this.getClass().getResource("/pictures/trump.wav"));
	        clip.open(stream);
	        clip.start();
		}
		catch (Exception e) {
			System.out.println("Can't find audio file");
		}
	}
	
}
