package carcassonne;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Tile {
	/**
	 * This variable holds the Follower placed (or not placed) on the Tile's Feature
	 */
	private Follower _follower;
	/**
	 * This array holds a combination of the x,y coordinates to place a follower image and the actual reference
	 * to the player's follower.
	 */
	private Object[][] _coordinates;
	/**
	 * Each index of _edges holds an array representing a side of the tile in the order [top,left,bottom,right]; 
	 * each index of an edge contains a Feature object (City, Field, Road)
	 */
	private Feature[][] _edges;
	/**
	 * Each index of _images holds an ImageIcon object representing a 90-degree clockwise
	 * rotation of the Tile's image.
	 */
	private ImageIcon[] _images;
	private ImageIcon _currentImage;
	/**
	 * Stores the index of the current rotation of the Tile's image
	 */
	private int _currentImageIndex;
	/**
	 * This method returns an the Tile edge corresponding with the direction index [top,left,bottom,right]
	 * @param direction is an integer representing the index associated with the location of the edge
	 * @return an array of Feature objects contained by the Tile edge
	 */
	public Feature[] getEdge(int direction){
		return _edges[direction];
	}
	/**
	 * This method accepts the predetermined Tile edge Feature objects defined in each 
	 * individual class and stores them in _edges.
	 * @param e is an array of arrays of Feature objects representing the predefined Tile edges
	 */
	public void initializeEdges(Feature[][] e){
		_edges = e;
	}
	/**
	 * This method accepts Strings representing file locations of the Tile images, converts them to 
	 * ImageIcon objects, and stores them in the instance variable _images.
	 * @param s is an array of String objects representing the file paths of the .png Tile images
	 * @throws IOException if the file can't be found (caught by each individually defined class)
	 */
	public void setImage(String[] s) throws IOException{
		_images = new ImageIcon[4];
		for (int i=0;i<s.length;i++){
			_images[i] = new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream(s[i])));
		}
		_currentImageIndex=0;
		_currentImage = _images[_currentImageIndex];
	}
	/**
	 * This method is called by the GUI and returns the icon pertaining to the current orientation of the tile.
	 * @return an ImageIcon object
	 */
	public ImageIcon getImage(){
		return _currentImage;
	}
	/**
	 * This method does, as its name suggests, a mod 4 rotation of the array of Tile edges and 
	 * sets the current image to the new orientation (90-degrees CW)
	 */
	public void rotate(){
		Feature[][] temp = new Feature[][]{_edges[3],_edges[0],_edges[1],_edges[2]};
		_edges=temp;
		_currentImageIndex = (_currentImageIndex+1)%4;
		resetCurrentImage();
		for (int i=0;i<_coordinates.length;i++){
			double x = (double)_coordinates[i][0];
			double y = (double)_coordinates[i][1];
			x = x - 0.5;
			y = 0.5 - y;
			double z = x;
			x = y;
			y = -z;
			x = x + 0.5;
			y = 0.5-y;
			_coordinates[i][0]=x;
			//System.out.println(_coordinates[i][0]);
			_coordinates[i][1]=y;
			//System.out.println(_coordinates[i][1]);
		}
	}
	/**
	 * This method is called by the Model to set the current image of the Tile orientation
	 * @param image of type ImageIcon is the new representation of the Tile's orientation
	 */
	public void setCurrentImage(ImageIcon image) {
		_currentImage = image;
	}
	
	public void resetCurrentImage() {
		_currentImage = _images[_currentImageIndex];
	}
	
	/**
	 * Once a valid Tile placement is selected and a Follower position is picked, the reference to
	 * the Follower is held by the variable _follower.
	 * @param f of type Follower holds the associated player's Follower.
	 */
	public void setFollower(Feature feature,Follower f){
		feature.getFollowers().add(f);
		_follower=f;
	}
	
	public void setFollower(Follower f){
		_follower=f;
	}
	
	/**
	 * Defined specifically for each different Tile, this holds the coordinates in which the Follower 
	 * images will be placed.
	 * @param coordinates of type Object[][] is an array of characteristic coordinates held in an array.
	 */
	public void setCoordinates(Object[][] coordinates) {
		_coordinates = coordinates;
	}
	/**
	 * A getter method for the Model that obtains the possible Follower image coordinates.
	 * @return the coordinates of the Follower image placement [x,y,Feature]
	 */
	public Object[][] getCoordinates() {
		return _coordinates;
	}

	public String tileName() {
		return this.getClass().getSimpleName()+_currentImageIndex;
	}
	
	public Follower getFollower() {
		return _follower;
	}
	
	public int getFollowerPosition() {
		for(int i=0; i<_edges.length; i++)
			for(int j=0; j<3; j++) {
				if (_edges[i][j] == _follower.getFeature()) return 3*i+j; 
			}
		return 12;
	}
	
	public void updateFollowersOnFeatures() {
		for(int i=0; i<_coordinates.length; i++) {
			Feature temp;
			temp = (Feature) _coordinates[i][2];
			temp.setFollowersOnAllSameFeatures();
		}
	}
	
}
