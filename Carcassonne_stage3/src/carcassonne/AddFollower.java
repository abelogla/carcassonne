package carcassonne;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

public class AddFollower implements ActionListener {
	/**
	 * A variable of type Tile to hold the tile passed by the button.
	 */
	private Tile _tile;
	/**
	 * A variable of type ImageIcon that holds the image of the current tile
	 * with a follower place on the appropriate feature.
	 */
	private ImageIcon _image;
	/**
	 * A variable of type Model that holds the reference to the Model of the game.
	 */
	private Model _model;
	/** 
	 * A variable of type Object that holds the Feature on which the follower is placed.
	 */
	private Feature _feature;
	/**
	 * This constructor creates an ActionListener for each placement choice for a follower,
	 * holding the variables for the Tile, Image, Feature, and Model associated with the game.
	 * @param model of type Model is the reference to the game's model.
	 * @param tile of type Tile is the reference to the current tile.
	 * @param image of type ImageIcon is the reference to the current image with placed follower.
	 * @param feature of type Object is the reference to the feature on which the follower is placed.
	 */
	public AddFollower(Model model,Tile tile,ImageIcon image,Object feature){
		_tile = tile;
		_image = image;
		_model = model;
		_feature = (Feature) feature;
	}
	/**
	 * This void method is called when the player has selected their placement of a follower, and 
	 * tells the model to continue and finish the move.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		_tile.setCurrentImage(_image);
		_model.finishMove(_feature);
	}

}
