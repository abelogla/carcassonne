package carcassonne;

public class Follower {
	/**
	 * Holds the reference to the Tile that the follower is placed on.
	 */
	private Tile _tile;
	/**
	 * Holds the reference to the Feature that the follower is placed on.
	 */
	private Feature _feature;
	/**
	 * Hold the reference to the Player the follower is associated with. 
	 */
	private Player _player;
	/**
	 * This constructor receives parameters that are characteristics of the follower and passes them as 
	 * instance variables held by the object.
	 * @param tile of type Tile is the Tile that the follower is placed on.
	 * @param feature of type Feature is the Feature that the follower is placed on.
	 * @param player of type Player is the Player the follower is associated with.
	 */
	public Follower(Tile tile, Feature feature, Player player){
		_tile = tile;
		_feature = feature;
		_player = player;
	}
	
	public Player getPlayer() {
		return _player;
	}
	
	public String getPlayerName() {
		return _player.getName();
	}
	
	public Feature getFeature() {
		return _feature;
	}
}
