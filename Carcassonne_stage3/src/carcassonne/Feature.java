package carcassonne;

import java.util.ArrayList;

public class Feature {
	
	protected ArrayList<Follower> _followers = new ArrayList<Follower>();
	
	public Feature() {

	}
	
	public int getOpennings() {
		return 1;
	}
	
	public void setOpennings(int opennings) {
		
	}
	
	public ArrayList<Tile> getTiles() {
		return null;
	}
	
	public void addTiles(ArrayList<Tile> tiles) {
		
	}
	
	public void setTiles(ArrayList<Tile> tiles) {
		
	}
	
	public ArrayList<Follower> getFollowers() {
		return _followers;
	}
	
	public void setFollowers(ArrayList<Follower> followers) {
		
	}
	
	public void setAllFollowers(ArrayList<Follower> followers) {
		
	}
	
	public void setFollowersOnAllSameFeatures() {
		
	}
	
	
	public ArrayList<Feature> getSameFeatures() {
		return null;
	}
	
	public void setSameFeatures(ArrayList<Feature> sameFeatures) {
		
	}
	
	public void updateNeighbors() {
		
	}
	
	public void updateOpennings(Tile t, int i) {
		
	}
	
	public int getNumberOfTiles() {
		return 1;
	}
	
	public int returnScore() {
		return 0;
	}
	
}
