package source;

public class dec2bin {
	static String result="";
	static public void recur(int number) {
		int remainder = number % 2;
//		System.out.println(remainder+"|");
		number /=2;
		if (number != 0) recur(number);
		result += ((remainder == 0) ? '0' : '1');
	}
	
/*	static public String decimal2bin(int number) {
		return recur(number);
	}
	*/
	static public void main(String[] args) {
		recur(90);
		System.out.println(result);
	}

}
