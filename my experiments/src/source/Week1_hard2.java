package source;

public class Week1_hard2 {
	public static void main(String[] args) {
		int[][] testerMatrix = new int[][]{
            { 1, 2, 3, 4, 5 },
            { 6, 7, 8, 9, 10 },
            { 11, 12, 13, 14, 15 },
            { 16, 17, 18, 19, 20 },
            { 21, 22, 23, 24, 25 }
		};

		printRowThenCol(testerMatrix);
	}

	static void printRowThenCol(int[][] mat) {
		for(int i=0; i<mat.length; i++) {
			for(int j=i; j<mat[i].length; j++) {
				System.out.print(mat[i][j]+" ");
			}
			System.out.println();
			if (i+1 <= mat[i].length) {
				for(int k=i+1; k<mat.length; k++) {
					System.out.print(mat[k][i]+" ");
				}
			}
			System.out.println();
		}
	}
}
