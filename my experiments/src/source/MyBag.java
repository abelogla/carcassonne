package source;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class MyBag<E> implements Collection<E> {
	
	E[] _bag;
	private int _size, _modCount;
	
	MyBag() {
		_size = 0;
		E[] _bag = (E[]) new Object[1];
		_modCount = 0;
	}
	
	private void resize(int size) {
		E[] temp = (E[]) new Object[size];
		for(int i=0; i<_bag.length; i++) {
			temp[i] = _bag[i];
		}
		_modCount++;
		_bag = temp;
	}

	@Override
	public int size() {
		return _size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return _size==0;
	}

	@Override
	public boolean contains(Object o) {
		if (o == null) {
			for(int i=0; i<_bag.length; i++)
				if(_bag[i] == null) return true;
		}
		else {
			for(int i=0; i<_bag.length; i++) {
				if (_bag[i].equals(o)) return true;
			}
		}
		return false;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new BagIt();
	}
	
	private class BagIt implements Iterator {
		
		int index,modCount;
		
		BagIt() {
			index=0;
			modCount = _modCount;
		}

		@Override
		public boolean hasNext() {
			if (modCount != _modCount)
				throw new ConcurrentModificationException();
			return index != _size;
		}

		@Override
		public Object next() {
			if (modCount != _modCount)
				throw new ConcurrentModificationException();
			if (index < _size) index++;
			//else throw new EmptyBagException();
			return _bag[index-1];
		}
		
	}
	
	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(E e) {
		if (_size == _bag.length) resize(_bag.length*2);
		_bag[_size] = e;
		_size++;
		_modCount++;
		return false;
	}

	@Override
	public boolean remove(Object o) {
		if (o == null) {
			for(int i=0; i<_bag.length; i++)
				if(_bag[i] == null) 
					return removeFromArray(i);
		}
		else {
			for(int i=0; i<_bag.length; i++) {
				if (_bag[i].equals(o)) 
					return removeFromArray(i);
			}
		}
		return false;
	}
	
	private boolean removeFromArray(int i) {
		_bag[i] = _bag[_size-1];
		_bag[_size-1] = null;
		_size--;
		_modCount++;
		return true;
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

}
