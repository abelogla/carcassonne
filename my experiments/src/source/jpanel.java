package source;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
//import javafx.scene.paint.Color;


public class jpanel extends JPanel {
	
	@Override
	public void paintComponent(Graphics h) {
	    int width = getWidth();
	    int height = getHeight();
	    h.setColor(Color.black);
	    h.drawOval(0, 0, width, height);
	  }
	
	  public static void main(String args[]) {
	    JFrame frame = new JFrame("Oval Sample");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.add(new jpanel());
	    frame.setSize(300, 200);
	    frame.setVisible(true);
	  }
}
