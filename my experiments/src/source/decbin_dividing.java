package source;

public class decbin_dividing {
	
	public static String dec2bin(int number) {
		String result="";
		int remainder=0, n=number;
		while (n != 0) {
			remainder = n % 2;
			result = (remainder == 0 ? "0" : "1") + result;
			n /= 2;
		}
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(dec2bin(9));
	}

}
