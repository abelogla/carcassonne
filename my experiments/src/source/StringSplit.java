package source;

import java.util.Arrays;

public class StringSplit {
	public static void main(String[] args) {
		String22Split();
	}
	
	static public void String1Split() {
		String s = "[vika,RED,0],[alex,BLUE,0]";
		String r[];
		r = s.split("(\\]\\,\\[)|\\[|\\]");
		r = Arrays.stream(r)
                .filter(o -> (o != null && o.length() > 0))
                .toArray(String[]::new); 
		for(String t:r)   
		System.out.println(t);
	}
	
	static public void String3Split() {
		String in = "V0,Q0,W0,L0,O0,D0,V0,I0,M0,H0,J0,B0,T0,U0,V0,E0,K0,U0,P0,W0,V0,D0,N0,V0,U0,I0,K0,P0,R0,H0,C0,W0,G0,B0,U0,V0,U0,U0,M0,L0,H0,V0,J0,R0,F0,S0,E0,P0,N0,U0,J0,N0,D0,W0,A0,R0,A0,E0,E0,O0,X0,V0,F0,S0";
		String[] r;
		r = in.split(",");
		for(String t:r)   
			System.out.println(t);
	}
	
	static public void String2Split() {
		String in = "U0(1,2)L0(2,2)[vika,5]E0(3,1)[vika,0]D0(3,2)B0(4,1)[alex,12]V0(5,1)[vika,0]K0(6,1)[alex,1]";
		String[] r;
		int m_counter=0, row=0;
		r = in.split("\\(|[\\)\\[]+|\\,(?!\\d+\\])|\\]");
		for(String st:r) {
			if(st.contains(",")) m_counter++;
		}
		String[][] ky = new String[(r.length-m_counter)/3][4];
		for(int i=0; i<r.length; i=i+3) {
			if(r[i].contains(",")) {
				ky[row-1][3] = r[i];
				i = i-2;
			}
			else {
				ky[row][0] = r[i];
				ky[row][1] = r[i+1];
				ky[row][2] = r[i+2];
				row++;
			}
		}
		for(String[] t:ky) {
			for(String g:t) {
				System.out.print(g+" ");
			}
			System.out.println("");
		}
	}
	
	static public void String22Split() {
		String in = "RA3(0,0)RD1(1,0)[amy,12]";
		String[] r;
		int m_counter=0, row=0;
		r = in.split("(\\]\\,)+|\\,(?!\\d+\\])|(\\)\\[)|\\(|(\\)\\,)+|\\]|\\)");
/*		for (String s:r) {
			System.out.println(s);
		}*/
		for(String st:r) {
			if(st.contains(",")) m_counter++;
		}
		String[][] ky = new String[(r.length-m_counter)/3][4];
		for(int i=0; i<r.length; i=i+3) {
			if(r[i].contains(",")) {
				ky[row-1][3] = r[i];
				i = i-2;
			}
			else {
				ky[row][0] = r[i];
				ky[row][1] = r[i+1];
				ky[row][2] = r[i+2];
				row++;
			}
		}
		for(String[] t:ky) {
			for(String g:t) {
				System.out.print(g+" ");
			}
			System.out.println("");
		}
	}
	
}
