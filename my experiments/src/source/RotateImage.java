package source;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class RotateImage extends JPanel {
	
	public static void main(String[] args) throws IOException {
	    JFrame frame = new JFrame("Test");

	    frame.add(new JComponent() {

	         BufferedImage image = ImageIO.read(new File("me.jpg"));

	         @Override
	         protected void paintComponent(Graphics g) {
	              super.paintComponent(g);

	              // create the transform, note that the transformations happen
	              // in reversed order (so check them backwards)
	              AffineTransform at = new AffineTransform();

	              // 4. translate it to the center of the component
	              at.translate(getWidth() / 2, getHeight() / 2);

	              // 3. do the actual rotation
	              at.rotate(Math.PI);

	              // 2. just a scale because this image is big
	              at.scale(0.5, 0.5);

	              // 1. translate the object so that you rotate it around the 
	              //    center (easier :))
	              at.translate(-image.getWidth()/2, -image.getHeight()/2);

	              // draw the image
	              Graphics2D g2d = (Graphics2D) g;
	              g2d.drawImage(image, at, null);

	              // continue drawing other stuff (non-transformed)
	              //...

	         }
	    });
	    frame.setVisible(true);
	}
}

