package source;

public class hex {
	
	public static int hex2dec(String number, int index) {
		int chartoint=0;
		index--;
		if (number.charAt(index) >= 'a' && number.charAt(index) <= 'z')
			chartoint = number.charAt(index) - 'a'+10;
		if (number.charAt(index) >= 'A' && number.charAt(index) <= 'Z')
			chartoint = number.charAt(index) - 'A'+10;
		if (number.charAt(index) >= '0' && number.charAt(index) <= '9')
			chartoint = number.charAt(index) - '0';
		if (index == 0) return chartoint;
		return chartoint + 16*hex2dec(number,index);
	}
	
	public static void main(String[] args) {
		System.out.println(hex2dec("1111",4));
	}

}