package source;

public class wup2 {

	/*
	 * This method takes as input a string which consists
	 * of several letter-digit pairs, such as "B1O2K1".
	 * 
	 * Define this method so that it returns a new String 
	 * which consists of the letters from input in the same
	 * order, but repeated the number of times indicated 
	 * by the following digit.
	 * 
	 * You may assume that the input String will always
	 * be valid input: a non-null String, with even length
	 * (possibly zero), consisting of alternating letters
	 * and digits.
	 * 
	 * Examples:
	 * solution("B1O2K1") must return "BOOK"
	 * solution("B1O2X0K1") must return "BOOK"
	 */

	public static String solution(String input) {
		String result="";
		for (int i=0; i<input.length(); i++) {
			if (input.charAt(i) >= 'A' && input.charAt(i) <= 'Z') {
					for (int j=0; j<input.charAt(i+1)-'0'; j++)
						result += input.charAt(i);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(solution("B1OX02K1"));

	}

}
