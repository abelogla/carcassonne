package code;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Player {
	
	public final String name;
	public final int age;
	public final Color color;
	private ArrayList<Meeple> meeples = new ArrayList<Meeple>();
	private int score = 0;
	
	Player(String _name,int _age, Color _color) {
		this.name = _name;
		this.age = _age;
		this.color = _color;
		while(meeples.size() < 7) meeples.add(new Meeple(this));
	}
	
	public Meeple giveMeeple() {
		if (meeples.size() == 0) {
			JOptionPane.showMessageDialog(null, "Sorry, you have no more meeples", "Message", JOptionPane.PLAIN_MESSAGE);
			return null;
		}
		return meeples.remove(meeples.size() - 1);
	}
	
	public void getMeepleBack(Meeple returningMeeple) {
		meeples.add(returningMeeple);
	}
	
	public int meeplesLeft() {
		return meeples.size();
	}
	
	public String getScoreString() {
		return score+"";
	}
	
	public int getScoreInt() {
		return score;
	}
	
	public void setScore(int _score) {
		score = _score;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
