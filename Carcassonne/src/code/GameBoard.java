package code;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class GameBoard {
	
	GameManager manager;
	ComparePlace cmp;
	ArrayList<JButton> board = new ArrayList<JButton>();
	ArrayList<JButton> options = new ArrayList<JButton>();
	JPanel grid;
	JPanel info;
	JPanel displayTile;
	JPanel scores;
	JPanel tileOptions;
	JScrollPane scroll;
	JFrame frame;
	JButton place;
	JButton rotate;
	JButton draw;
	JButton end;
	JButton meeple;
	JLabel currentTile;
	JLabel playerScores;
	TileBag bag;
	Tile drawnTile;
	ImageIcon currentIcon;
	int currentIndex = 144;
	BoardEvent buttonListener;
	String playerScore;
	boolean tilePlaced = false;
	boolean placedMeeple = false;
	
	public GameBoard(GameManager manage, ComparePlace cp) {
		
		cmp = cp;
		manager = manage;
		buttonListener = new BoardEvent(this, manager, cmp);
		
		frame = new JFrame();
		grid = new JPanel(new GridLayout(17,17));
		info = new JPanel(new GridLayout(3, 1));
		displayTile = new JPanel();
		scores = new JPanel();
		tileOptions = new JPanel();
		scroll = new JScrollPane(grid, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.setPlayerScorePanel();
		playerScores = new JLabel(playerScore);
		currentTile = new JLabel();
		
		this.fillBoard();
		this.setOptionButtons();
		this.setGameBoard();
		this.setStartTile();

	}

	public void setTile(){
		
		if(tilePlaced == false){
			board.get(currentIndex).setIcon(manager.getTileIcon());
			board.get(currentIndex).setVisible(true);
			board.get(currentIndex).removeMouseListener(buttonListener);
			end.setVisible(true);
			meeple.setVisible(true);
			place.setVisible(false);
			rotate.setVisible(false);
			tilePlaced = true;
		}
	}
	
	public void fillBoard(){
		
		for(int i = 0; i < 289; i++){
			JButton button = new JButton();
			button.setVisible(false);
			button.addMouseListener(buttonListener);
			grid.add(button);
			board.add(button);
		}	
	}
	
	public void setPlayerScorePanel(){
		
		playerScore = "<html>Player Scores: <BR>" +manager.getPlayerArray()[0].toString()+"<BR>";
		for(int i = 1; i < manager.getPlayerCount(); i++){
			playerScore = playerScore+manager.getPlayerArray()[i].toString()+"<BR>";
		}
		playerScore = playerScore+"</html>";
	}
	
	public void setOptionButtons(){
		
		end = new JButton("End Turn");
		end.addMouseListener(new EndTurnEvent(this, manager));
		end.setVisible(false);
		
		place = new JButton("Place Tile");
		place.addMouseListener(new PlaceTileEvent(this));
		place.setVisible(false);
		
		draw = new JButton("Draw Tile");
		draw.addMouseListener(new DrawTileEvent(manager, this));
		
		rotate = new JButton("Rotate Tile");
		rotate.addMouseListener(new RotateTileEvent(manager, this));
		rotate.setVisible(false);
		
		meeple = new JButton("Place Meeple");
		meeple.addMouseListener(new PlaceMeepleEvent(manager, this));
		meeple.setVisible(false);
		
		options.add(place);
		options.add(rotate);
		options.add(draw);
		options.add(end);
		options.add(meeple);
	}
	
	public void setGameBoard(){
		
		info.setPreferredSize(new Dimension(300, 50));
		
		scores.add(playerScores);
		displayTile.add(currentTile);
		displayTile.setBackground(new Color(218,165,32));
		playerScores.setFont(new Font("MonoSpace", Font.BOLD, 18 ));
		
		tileOptions.setBackground(new Color(218,165,32));
		tileOptions.add(place);
		tileOptions.add(rotate);
		tileOptions.add(meeple);
		tileOptions.add(draw);
		tileOptions.add(end);
		
		info.add(scores);
		info.add(displayTile);
		info.add(tileOptions);
		
		playerScores.setVisible(true);
		grid.setPreferredSize(new Dimension(1400,1400));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(scroll, BorderLayout.CENTER);
		frame.getContentPane().add(info, BorderLayout.EAST);
		frame.setSize(1800,1800);
		frame.setVisible(true);
		grid.setVisible(true);
		info.setVisible(true);
		grid.setBackground(new Color(101, 143, 49));
		grid.setOpaque(true);
		info.setBackground(new Color(218,165,32));
		info.setOpaque(true);
		scores.setBackground(new Color(218,165,32));
		scores.setOpaque(true);
	}
	
	public void setStartTile(){
		
		ImageIcon startIcon = new ImageIcon("resources/11a.jpg");
		board.get(144).setBackground(new Color(218,165,32));
		board.get(144).setOpaque(true);
		board.get(144).setIcon(startIcon);
		board.get(144).setVisible(true);
	}
	
	public void resetTilePlaced(){
		
		tilePlaced = false;
	}
	
	public void placedMeeple(){
		
		placedMeeple = true;	
	}
	
	public void resetMeeple(){
		
		placedMeeple = false;
	}
	
	public boolean hasPlacedMeeple(){
		
		return placedMeeple;
	}
	
	public void setCurrentIcon(){
		
		currentTile.setIcon(manager.getTileIcon());
		currentTile.setVisible(true);
	}
	
	public void removeCurrentIcon(){
		
		currentTile.setVisible(false);
		
	}
	
	public int getCurrentIndex(){
		
		return currentIndex;
	}

	public void setCurrentIndex(JButton button){
		
		currentIndex = board.indexOf(button);
		manager.setCurrentIndex(currentIndex);
	}
	
	public JButton optionButton(int index){
		
		return options.get(index);
	}
	
	public void setVisible(int index){
		
		if(board.get(index).isVisible() == false){
			board.get(index).setVisible(true);
			board.get(index).setBackground(new Color(218,165,32));
			board.get(index).setOpaque(true);
		}
		
	}
	
	public void updateBoard(){

		if(currentIndex > 17 && currentIndex < 272 && currentIndex%17 > 0 && currentIndex%17 < 16){

			int index = currentIndex + 17;
			setVisible(index);
			
			index = currentIndex - 17;
			setVisible(index);
				
			index = currentIndex + 1;
			setVisible(index);
			
			index = currentIndex - 1;
			setVisible(index);
		}

	}
}
