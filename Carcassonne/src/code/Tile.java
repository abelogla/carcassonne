package code;

import java.util.ArrayList;

import javax.swing.ImageIcon;

/** Tile
 * 
 * Tile consists of four objects of class Side placed into array "sides" and certain number of objects of
 * type Terrain and Pointer.
 * Each side contains three instance variables of type Pointer placed into array "parts".
 * Each of those variables represents a certain part of a side (each side has three parts).
 * Each of the terrains on tile has its own pointer.
 * If, for example, tile has three terrains: road and two fields, then Tile object contains four objects of
 * type Side, three objects of type Terrain and three objects of type Pointer, each of which contains
 * reference to corresponding object of type Terrain. If one of those field is presented on north side
 * then all three parts of north side will contain reference to Pointer object associated with Terrain object
 * which represents this field. Once reference to Terrain object is changed in Pointer object, all side
 * parts contained reference to this Pointer object will point out to different Terrain object.
 * 
 * Tile object is created based on configuration string passed as argument to constructor.
 * Configuration string contains one capital character and 12 digits.
 * F - Terrain object is field
 * C - Terrain object is city
 * S - Terrain object is city with shield
 * R - Terrain object is road
 * M - Terrain object is cloister
 * Next twelve digits show the side and exact part where terrain is presented:
 * First three digits - NORTH, next three digits - EAST, rest - SOUTH and WEST.
 * If value is 1 - terrain occupies that part.
 * Configuration string for starting tile: C111000000000F000100000001R000010000010F000001111100
 */

public class Tile implements MainInterface {		
	
	private ImageIcon picture;
	private int state_index = 0;
	private String image_name;
	private Side[] sides = new Side[N_OF_SIDES];
	
	// Array of all pointers (terrains) on tile
	private ArrayList<Pointer> allPointersOnTile = new ArrayList<Pointer>(1);
	
	// Tile constructor creates a new tile based on
	// configuration string
	public Tile(String tile_configuration, String _image_name) {
		// Name of corresponding image for tile
		image_name = _image_name;
		// Set unrotated icon
		rotateIcon();
		// Keeps track of a side to parse 
		int current_side = 0;
		// Counts parts of the tile that terrain occupies
		int sections_occupied = 0;
		// Indicates whether or not city contains the shield sign
		int shield = 0;
		// Name of terrain
		String type="";
		// Creating objects of type Side
		for(int j=0;j<N_OF_SIDES;j++)
			sides[j] = new Side();
		// Parsing of configuration string
		for(int i=1; i<tile_configuration.length(); i+=13) {
			if (tile_configuration.charAt(i) == 'F') type = FIELD;
			if (tile_configuration.charAt(i) == 'C') type = CITY;
			if (tile_configuration.charAt(i) == 'S') { type = CITY; shield = 1; }
			if (tile_configuration.charAt(i) == 'R') type = ROAD;
			if (tile_configuration.charAt(i) == 'M') { type = CLOISTER; }
			Terrain new_terrain = new Terrain(shield, type);
			Pointer new_pointer = new Pointer(new_terrain, tile_configuration.substring(i+1, i+13));
			new_terrain.allPointersToMe.add(new_pointer);
			new_terrain.allTilesImOn.add(this);
			allPointersOnTile.add(new_pointer);
			for(int j=i+1; j<i+13; j+=3) {
				for(int k=j; k<j+3; k++) {
					if (tile_configuration.charAt(k) == '1') {
						sections_occupied++;
						// if North or East, put terrains in the same order
						if (current_side < SOUTH)
							sides[current_side].parts[k-j] = new_pointer;
						// if South or West, terrains should be put in reverse order
						else sides[current_side].parts[(j-k)+2] = new_pointer;
					}
				}
				current_side++;
			}
			new_pointer.getTerrain().setNumberOfConnectableSections(sections_occupied);
			sections_occupied = 0;
			shield = 0;
			current_side = 0;
		}
	}
	
	// Rotates the tile clockwise
	// For North and South reverses the array of pointers to correct the order
	public void rotateTile() {
		Side[] temp = new Side[N_OF_SIDES];
		temp[NORTH] = sides[WEST].reverseSide();
		temp[EAST] = sides[NORTH];
		temp[SOUTH] = sides[EAST].reverseSide();
		temp[WEST] = sides[SOUTH];
		sides = temp;
		rotateIcon();
		rotateCoordinates();
	}
	
	// Returns image of the tile
	public ImageIcon getImage(){
		return  picture;
	}
	
	// Helper method sets ImageIcon based on tile orientation
	private void rotateIcon() {
		char[] tile_rotation_state = {'a', 'b', 'c', 'd'};
		picture = new ImageIcon("resources/" + image_name + tile_rotation_state[state_index%4]+".jpg");
		state_index++;
	}
	
	// Helper method rotates coordinates of all terrains
	private void rotateCoordinates() {
		for(Pointer p : allPointersOnTile) 
			p.rotateCoordinates();
	}
	
	// Method returns an array of terrains for specific SIDE
	public Terrain[] getTerrainsOnSide(int SIDE) {
		return sides[SIDE].arrayOfTerrains();
	}
	
	// Method returns an array of pointers of all terrains on the tile
	public ArrayList<Pointer> getAllPointersOnTile() {
		return allPointersOnTile;
	}
	
	// Returns string of coordinates for a given Terrain object
	public String getTerrainCoordinates(Terrain terrainToLookFor) {
		for(Pointer p: allPointersOnTile)
			if (p.getTerrain() == terrainToLookFor) return p.getCoordinates();
		return null;
	}
	
	// Returns array of names of all terrains on the tile with abbreviation of sides they are on
	public String[] getTerrainsNames() {
		String[] names = new String[getAllPointersOnTile().size()];
		// Iterating through all pointers on tile to get terrains
		for(int i=0; i<allPointersOnTile.size();  i++) {
			String abbreviation = "";
			// Iterating through the sides to find out if current terrain is on side
			for(int j=NORTH; j<N_OF_SIDES; j++) {
				int onSide = -1;
				// Actual checking
				for(int k=0; k<getTerrainsOnSide(j).length; k++)
					if (allPointersOnTile.get(i).getTerrain().equals(getTerrainsOnSide(j)[k])) onSide = j;
				if(onSide == NORTH) abbreviation += 'N'; if(onSide == EAST) abbreviation += 'E';
				if(onSide == SOUTH) abbreviation += 'S'; if(onSide == WEST) abbreviation += 'W';
			}
			names[i] = allPointersOnTile.get(i).getTerrain().type +" ["+abbreviation+"]";
		}
		return names;
	}
	
}
