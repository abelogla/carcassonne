package code;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class RotateTileEvent implements MouseListener {

	
	GameManager manager;
	GameBoard board;
	
	public RotateTileEvent(GameManager m, GameBoard b){
		board = b;
		manager = m;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		manager.getCurrentTile().rotateTile();
		board.setCurrentIcon();

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
