package code;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class ComparePlace implements MainInterface {
	
	// Method executes when a player tries to place tile on board at certain index.
	// At first it checks if current tile can actually be placed by comparing each side
	// with sides of surrounding tiles. If it is possible, connects all sides to other tiles and
	// places current tile in the array of tiles.
	// Index of tiles to compare with is calculated based on board size
	public boolean place(Tile[] boardArray, Tile drawnTile, int index) {
		int boardSize = boardArray.length;
		int boardSideSize = (int)Math.sqrt(boardSize);
		int placeble = 0;
		// Comparing every side of drawn tile to corresponding sides of adjacent tiles
		placeble += compare(drawnTile, NORTH, boardArray[index-boardSideSize]);
		placeble += compare(drawnTile, SOUTH, boardArray[index+boardSideSize]);
		placeble += compare(drawnTile, EAST, boardArray[index+1]);
		placeble += compare(drawnTile, WEST, boardArray[index-1]);
		// If tile can be placed, connecting each side one by one
		if(placeble == 0) {
			connect(drawnTile, NORTH, boardArray[index-boardSideSize]);
			connect(drawnTile, SOUTH, boardArray[index+boardSideSize]);
			connect(drawnTile, EAST, boardArray[index+1]);
			connect(drawnTile, WEST, boardArray[index-1]);
			boardArray[index] = drawnTile;
		}
		else {
			JOptionPane.showMessageDialog(null, "Illegal move! Choose other spot!", "Message", JOptionPane.PLAIN_MESSAGE);
			return false;
		}
		return true;
	}	
	
	// Helper method compares SIDE of drawn tile with the corresponding side of given tile
	private int compare(Tile drawnTile, int SIDE, Tile comparing_tile) {
		if (comparing_tile != null) {
			return (drawnTile.getTerrainsOnSide(SIDE)[MIDDLE].type == comparing_tile.getTerrainsOnSide((SIDE+2)%4)[MIDDLE].type) ? 0 : 1;
		}
		else return 0;
	}
	
	// Helper method connects SIDE of drawn tile to the corresponding side of given tile
	private void connect(Tile drawnTile, int SIDE, Tile toTile) {
		if (toTile != null) {
			for(int section=LEFTMOST; section<N_OF_SECTIONS; section++) {
				// Connecting sections of side of drawn tile to corresponding sections of other tile.
				// Updating Terrain properties, but only if sections point out to not the same Terrain object
				if (drawnTile.getTerrainsOnSide(SIDE)[section] != toTile.getTerrainsOnSide((SIDE+2)%4)[section]) {
					toTile.getTerrainsOnSide((SIDE+2)%4)[section].numberOfConnectableSections = 
							(drawnTile.getTerrainsOnSide(SIDE)[section].numberOfConnectableSections - 1) +
							(toTile.getTerrainsOnSide((SIDE+2)%4)[section].numberOfConnectableSections - 1);
					toTile.getTerrainsOnSide((SIDE+2)%4)[section].shields =
							drawnTile.getTerrainsOnSide(SIDE)[section].shields + toTile.getTerrainsOnSide((SIDE+2)%4)[section].shields;
					toTile.getTerrainsOnSide((SIDE+2)%4)[section].supply = 
							drawnTile.getTerrainsOnSide(SIDE)[section].supply + toTile.getTerrainsOnSide((SIDE+2)%4)[section].supply;
					toTile.getTerrainsOnSide((SIDE+2)%4)[section].meeples.addAll(drawnTile.getTerrainsOnSide(SIDE)[section].meeples);
					toTile.getTerrainsOnSide((SIDE+2)%4)[section].allPointersToMe.addAll(drawnTile.getTerrainsOnSide(SIDE)[section].allPointersToMe);
					toTile.getTerrainsOnSide((SIDE+2)%4)[section].allTilesImOn.addAll(drawnTile.getTerrainsOnSide(SIDE)[section].allTilesImOn);
					for(Pointer p : drawnTile.getTerrainsOnSide(SIDE)[section].allPointersToMe)
						p.set_pointsTo(toTile.getTerrainsOnSide((SIDE+2)%4)[section]);
				}
				else {
					// If connecting sections point out to the same Terrain object then just decrement field
					// "numberOfConnectableSections" by 2, which means the terrain now is presented on less number
					// of sections. If "numberOfConnectableSections" equals to "0" then terrain is completed.
					drawnTile.getTerrainsOnSide(SIDE)[section].numberOfConnectableSections -= 2;
					
					// If completed terrain is city then looking for adjacent fields and increment the variable
					// "supply" by one, which means those fields are supplying completed city.
					// Searching for adjacent fields is based on string of coordinates that every Pointer object has
					if (drawnTile.getTerrainsOnSide(SIDE)[section].type == CITY && 
							drawnTile.getTerrainsOnSide(SIDE)[section].isCompleted()) {
						for(Tile tile : drawnTile.getTerrainsOnSide(SIDE)[section].allTilesImOn) {
							String coordinates = tile.getTerrainCoordinates(drawnTile.getTerrainsOnSide(SIDE)[section]);
							ArrayList<Integer> indexes = new ArrayList<Integer>(1);
							for(int i=1; i< coordinates.length()-1; i++) {
								if (coordinates.charAt(i) == '0' &&
										(coordinates.charAt(i-1) == '1' || coordinates.charAt(i+1) == '1'))
									indexes.add(i);
								if (coordinates.charAt(0) == '0' && coordinates.charAt(coordinates.length()-1) == '1')
									indexes.add(0);
								if (coordinates.charAt(0) == '1' && coordinates.charAt(coordinates.length()-1) == '0')
									indexes.add(coordinates.length()-1);
							}
							ArrayList<Terrain> changedTerrains = new ArrayList<Terrain>(1);
							for(Pointer p : tile.getAllPointersOnTile()) {
								for(int i=0; i<p.getCoordinates().length(); i++) {
									if (p.getCoordinates().charAt(i) == '1' && indexes.contains(i)) {
										if (p.getTerrain().type == FIELD && !changedTerrains.contains(p.getTerrain())) {
											p.getTerrain().supply++;
											changedTerrains.add(p.getTerrain());
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
