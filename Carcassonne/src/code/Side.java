package code;

public class Side implements MainInterface {

	// ArrayLists of side's parts
	public Pointer[] parts = new Pointer[N_OF_SECTIONS];

	// Returns an array of terrains on the side
	public Terrain[] arrayOfTerrains() {
		Terrain[] terrains = new Terrain[N_OF_SECTIONS];
		for(int i=LEFTMOST; i<N_OF_SECTIONS; i++) {
			terrains[i] = parts[i].getTerrain();
		}
		return terrains;
	}
	
	// Changes the order of terrains in the array allTerrains
	// Needed to rotate the tile
	public Side reverseSide() {
		Pointer temp;
		temp = parts[LEFTMOST];
		parts[LEFTMOST] = parts[RIGHTMOST];
		parts[RIGHTMOST] = temp;
		return this;
	}
}
