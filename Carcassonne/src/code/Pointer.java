package code;

public class Pointer {
	private Terrain terrainItPointsTo;
	private String coordinates;
		
	Pointer(Terrain terrain, String _coordinates) {
		set_pointsTo(terrain);
		this.coordinates = _coordinates;
	}
	
	// Returns the actual Terrain object the pointer points to
	public Terrain getTerrain() {
		return terrainItPointsTo;
	}
	
	// Set Terrain object the pointer points to
	public void set_pointsTo(Terrain terrain) {
		this.terrainItPointsTo = terrain;
	}
	
	// Returns the string of coordinates
	public String getCoordinates() {
		return coordinates;
	}
	
	// Shifting the string of coordinates by 3 when rotating tile
	public void rotateCoordinates() {
		String temp;
		temp = coordinates.substring(9) + coordinates.substring(0, 9);
		coordinates = temp;
	}
	
}
