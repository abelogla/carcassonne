package code;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class TileBag {
	
	ArrayList<Tile> newBag;
	
	// Fill up the bag of tiles
	public void generateTiles() {
		String config_line, image_name;
		int numberOfTiles;
		newBag = new ArrayList<Tile>(71);
		try {
			Scanner file_scanner = new Scanner(new FileReader("resources/tiles.cfg"));
			while (file_scanner.hasNext()) {
				image_name = file_scanner.next();
				config_line = file_scanner.next();
				numberOfTiles = Character.getNumericValue(config_line.charAt(0));
				for(int i=0; i<numberOfTiles; i++) newBag.add(new Tile(config_line, image_name));
			}
			file_scanner.close();
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	// Draw random tile from the bag
	public Tile getRandomTile() {
		Random randomInt = new Random();
		Tile drawnTile;
		int randomIndex = randomInt.nextInt(newBag.size());
		drawnTile = newBag.get(randomIndex);
		newBag.remove(randomIndex);
		return drawnTile;
	}
	
	// Create the start tile and return a reference to it
	public Tile getStartTile() {
		return new Tile("1C111000000000F000100000001R000010000010F000001111100", "11");
	}
	
}