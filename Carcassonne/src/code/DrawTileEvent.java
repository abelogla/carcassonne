package code;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class DrawTileEvent implements MouseListener {

	GameBoard board;
	GameManager manager;
	
	public DrawTileEvent(GameManager m, GameBoard b){
		
		board = b; 
		manager = m;
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e){
		
		manager.drawTile();
		board.setCurrentIcon();
		board.optionButton(2).setVisible(false);
		board.optionButton(0).setVisible(true);
		board.optionButton(1).setVisible(true);
		
		
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
