package code;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TempGameBoard {

	public TempGameBoard(){
		
		JFrame frame = new JFrame();
		JPanel grid = new JPanel(new GridLayout(9,9));
		JPanel scores = new JPanel(new FlowLayout());
		JButton crntTile = new JButton("tile image");
		scores.setPreferredSize(new Dimension(300, 1200));
		scores.setLayout(new GridLayout(0,1));
		grid.setPreferredSize(new Dimension(900, 1200));
		JLabel playerScores = new JLabel("<html>PLAYER SCORES: <BR><BR>Player 1:<BR>Player 2:<BR>Player 3:<BR></html>");
		scores.add(playerScores);
		scores.add(crntTile);
		playerScores.setVisible(true);
		frame.setSize(1200,1200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(grid, BorderLayout.WEST);
		frame.getContentPane().add(scores, BorderLayout.EAST);
		frame.setVisible(true);
		grid.setVisible(true);
		scores.setVisible(true);
		
		
	
		
		for(int i = 0; i < 81; i++){
			
			JButton button = new JButton();
			button.setVisible(true);
			grid.add(button);
			
		}	
	}
}
