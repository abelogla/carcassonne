package code;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PlaceTileEvent implements MouseListener{

	GameBoard board;
	
	public PlaceTileEvent(GameBoard b){
		
		board = b;
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		
		board.updateBoard();
		board.optionButton(0).setVisible(false);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
