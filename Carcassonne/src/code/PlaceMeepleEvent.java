package code;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PlaceMeepleEvent implements MouseListener {

	GameManager manager;
	GameBoard board;
	
	public PlaceMeepleEvent(GameManager m, GameBoard b){
		
		manager = m; 
		board = b;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		manager.placeMeeple();
		board.optionButton(4).setVisible(false);
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
