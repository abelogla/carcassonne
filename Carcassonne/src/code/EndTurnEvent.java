package code;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class EndTurnEvent implements MouseListener {

	GameBoard board;
	GameManager manager;
	
	public EndTurnEvent(GameBoard b, GameManager m){
		
		manager = m;
		board = b;
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		manager.scoreCompletedTerrains();
		board.resetTilePlaced();
		board.removeCurrentIcon();
		board.optionButton(3).setVisible(false);
		board.optionButton(2).setVisible(true);
		board.optionButton(0).setVisible(false);
		board.optionButton(1).setVisible(false);
		board.optionButton(4).setVisible(false);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
