package code;

public interface MainInterface {
	
	int NORTH = 0;
	final int EAST = 1;
	final int SOUTH = 2;
	final int WEST = 3;
	final int N_OF_SIDES = 4;
	final int N_OF_SECTIONS = 3;
	final int LEFTMOST = 0;
	final int MIDDLE = 1;
	final int RIGHTMOST = 2;
	final String FIELD = "Field";
	final String ROAD = "Road";
	final String CITY = "City";
	final String CLOISTER = "Cloister";
	
}
