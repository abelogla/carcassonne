package code;

public class Meeple {
	
	public final Player player;
	
	public Meeple(Player _player) {
		this.player = _player;
	}
	
	// Get this meeple back to the owner
	public void goBackToPlayer() {
		player.getMeepleBack(this);
	}
}
