package code;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

//The Game Manager Class creates an array of Tiles that mirrors the actual game board. The methods send 
//information to the board and retrieves information to send to the event handlers and the compare class.
//the Tile Grid array is set to hold 289 elements that represents a 17x17 game board.
//The actual board is 15x15 so the extra row and column acts as a buffer to make comparing tiles on the
//board easier.


public class GameManager {

	Tile currentTile;
	GameBoard board;
	int currentIndex = 144; 
	int playerCount;
	Tile[] tileGrid;
	Player[] players;
	Player currentPlayer;
	TileBag bag;
	
	
	public GameManager(){
		
		this.startGame();	
		bag = new TileBag();	// creates a new Tile Bag
		bag.generateTiles();	// and fills an array with the game tiles
		
		
		tileGrid = new Tile[289];	//this array will hold the Tile objects
		
			
		tileGrid[144] = bag.getStartTile();	//puts the start tile into the tile grid
		
	}
	
	public void placeMeeple(){
		
		// Creates an array of the terrains found in a given Tile and creates a dialog window
		// The option buttons
		
		String [] terrains = currentTile.getTerrainsNames();
		Meeple placingMeeple;
		int n = JOptionPane.showOptionDialog(null,
			    "Where do you want to place Meeple?",
			    "Choose a terrain",
			    JOptionPane.DEFAULT_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    terrains,
			    terrains[0]);
		if(!currentTile.getAllPointersOnTile().get(n).getTerrain().hasMeepleOfOtherPlayer(currentPlayer)) {
			placingMeeple = currentPlayer.giveMeeple();
			if (placingMeeple != null){
				currentTile.getAllPointersOnTile().get(n).getTerrain().meeples.add(placingMeeple);
				//board.placedMeeple();
			}
		}
		else
			JOptionPane.showMessageDialog(null, "Sorry, it is occupied already!", "Message", JOptionPane.PLAIN_MESSAGE);
		scoreCompletedTerrains();
	}
	
	// Method scores all completed Roads/Cities and returns meeples back to owners
	public void scoreCompletedTerrains() {
		for(Pointer p: currentTile.getAllPointersOnTile()) {
			if(p.getTerrain().isCompleted() && (p.getTerrain().type == "Road" || p.getTerrain().type == "City")) {
				int [] playersMeeples = new int[players.length];
				for(int i=0; i<players.length; i++) {
					playersMeeples[i] = p.getTerrain().numberOfMeeplesOfPlayer(players[i]);
				}
				int maxMeeples=playersMeeples[0], index=0;
				for(int i=1; i<players.length; i++) {
					if(maxMeeples < playersMeeples[i]) index = i;
				}
				if(maxMeeples == 1) {
					for(int i=0; i<players.length; i++) {
						if(playersMeeples[i] == 1) {
							players[i].setScore(players[i].getScoreInt() + p.getTerrain().returnScore());
							p.getTerrain().removeMeeplesOfPlayer(players[i]);
						}
					}
				}
				if(maxMeeples > 1) {
					players[index].setScore(players[index].getScoreInt() + p.getTerrain().returnScore());
					for(int i=0; i<players.length; i++) {
						p.getTerrain().removeMeeplesOfPlayer(players[i]);
					}
				}
				JOptionPane.showMessageDialog(null, "Current player "+currentPlayer.name+" has "+currentPlayer.meeplesLeft()+" meeples and"+" score: "+currentPlayer.getScoreString(), "Message", JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
	
	public void startTurn(){
		
		//currentPlayer = players.get(0);
	}
	
	public void endTurn(){

		
	}

	public void startGame(){
		
		PlayerInfo info = new PlayerInfo();
		players = info.get();
		playerCount = players.length;
		currentPlayer = players[info.getFirstPlayerIndex(players)];
		
	}
	
	public Player[]	getPlayerArray(){
		
		return players;
	}
	
	public int getPlayerCount(){
		
		return playerCount;
	}

	public void placeTile(){
		
		tileGrid[currentIndex] = currentTile;
		
	}
	
	public void drawTile(){
		
		currentTile = bag.getRandomTile();
	}

	public void setCurrentIndex(int index){
		
		currentIndex = index;
		
	}
	
	public Tile getCurrentTile(){
		
		return currentTile;
		
	}
	
	public int getCurrentIndex(){
		
		return currentIndex;
	}
	
	public Tile[] getTileGrid(){
		
		return tileGrid;
	}
	
	public ImageIcon getTileIcon(){
		
		return currentTile.getImage();
	}
	
	
	
	
}
