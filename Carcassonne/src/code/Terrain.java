package code;

import java.util.ArrayList;

public class Terrain implements MainInterface {
	
	// ArrayList of meeples on terrain
	public ArrayList<Meeple> meeples = new ArrayList<Meeple>(1);
	
	// Name of the terrain
	public final String type;
	
	// Array of pointers to this terrain
	ArrayList<Pointer> allPointersToMe = new ArrayList<Pointer>(1);
	
	// Array of tiles this terrain is on
	ArrayList<Tile> allTilesImOn = new ArrayList<Tile>(1);
	
	// Represents the number of sections this terrain occupies.
	// Needed to determine whether or not a terrain is completed
	int numberOfConnectableSections;
	
	// How many cities are supplied
	int supply = 0;
	
	// How many shields this terrain contains? Cities may contain 1+
	public int shields = 0;
	
	// Constructor
	Terrain(int _shields, String _type) {
		this.type = _type;
		this.shields = _shields;
	}
	
	// Is terrain completed?
	public boolean isCompleted() {
		return (numberOfConnectableSections == 0) ? true : false;
	}
	
	public void setNumberOfConnectableSections(int _numberOfConnectableSections) {
		numberOfConnectableSections = _numberOfConnectableSections;
	}
	
	// Returns true if terrain has meeples of other players
	public boolean hasMeepleOfOtherPlayer(Player player) {
		for(Meeple m: meeples)
			if (m.player != player) return true;
		return false;
	}
	
	// Returns number of meeples given player has on terrain
	public int numberOfMeeplesOfPlayer(Player player) {
		int n=0;
		for(Meeple m: meeples)
			if (m.player == player) n++;
		return n;
	}

	// Returns all meeples of given player back
	public void removeMeeplesOfPlayer(Player player) {
		ArrayList<Meeple> newMeeples = new ArrayList<Meeple>(1);
		for(Meeple m: meeples) {
			if (m.player != player) newMeeples.add(m); 
			else m.goBackToPlayer();
		}
		meeples = newMeeples;
	}

	// Returns score
	public int returnScore() {
		int numberOfTilesTerrainIsOn = allTilesImOn.size();
		switch(type) {
		case FIELD:
			return 4*supply;
		case ROAD:
			System.out.println(numberOfTilesTerrainIsOn);
			return numberOfTilesTerrainIsOn;
		case CITY:
			if(isCompleted()) {
				return (numberOfTilesTerrainIsOn > 2) ? numberOfTilesTerrainIsOn*2 + shields*2 : 2 + shields*2;
			}
			return numberOfTilesTerrainIsOn + shields;
		case CLOISTER:
			return 9;
		}
		return 0;
	}
	
}
