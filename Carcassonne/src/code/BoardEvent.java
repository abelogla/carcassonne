package code;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

public class BoardEvent implements MouseListener{

	GameBoard board;
	GameManager manager;
	ComparePlace compPlace;
	
	
	public BoardEvent(GameBoard b, GameManager m, ComparePlace cp){
		
		board = b;
		manager = m;
		compPlace = cp;
		
	}
	
	//added comment
	@Override
	public void mouseClicked(MouseEvent e) {
		
		
		JButton button = (JButton) e.getSource();
		board.setCurrentIndex(button);
		if(compPlace.place(manager.getTileGrid(), manager.getCurrentTile(), manager.getCurrentIndex())){
			board.setTile();
		}
		
	
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
