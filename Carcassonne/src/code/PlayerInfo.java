package code;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PlayerInfo {
	
	private static String[] colors = new String[] {"Pink", "Blue",
			"Yellow", "Green", "Red", "Orange"};
	private static JComboBox<String> colorsList = new JComboBox<String>(colors);
	private static JTextField nameField = new JTextField();
	private static JTextField ageField = new JTextField();
    
	public Player[] get() {
		ArrayList<Color> colors = new ArrayList<Color>(6);
		Player[] players;
		int age = 0;
		int number = 0;
		colors.add(Color.PINK);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.GREEN);
		colors.add(Color.RED);
		colors.add(Color.ORANGE);
		// Getting correct number of players
		while(number < 2 || number > 5) {
			System.out.println("Entrance to while loop");
			try {
				String returnString;
				returnString = JOptionPane.showInputDialog(null, "Enter number of players [ 2 - 5 ] :", "Message", JOptionPane.PLAIN_MESSAGE);
				if (returnString == null) System.exit(0);
				number = Integer.parseInt(returnString);
			}
			catch(Exception e) { number = 0; }
			System.out.println("End of while loop");
		}
		// Creating an array of players
		players = new Player[number];
		// Filling it up
		for(int i=0; i<number; i++) {
			while (age < 10 || age > 100 || nameField.getText().length() == 0) {
				ageField.setText("");
				int feedback = JOptionPane.showConfirmDialog(null, getPanel(), "Player "+(i+1), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				if (feedback == JOptionPane.CANCEL_OPTION) System.exit(0);
				else if (feedback == JOptionPane.OK_OPTION) {
					try {
						age = Integer.parseInt(ageField.getText());
					}
					catch(Exception e) { age = 0; }
					if (age < 10 || age > 100) ageField.setBackground(new Color(255,230,230));
					if (nameField.getText().length() == 0) nameField.setBackground(new Color(255,230,230));
					else nameField.setBackground(Color.WHITE);
				}
			}
			players[i] = new Player(nameField.getText(), age, colors.get(colorsList.getSelectedIndex()));
			colors.remove(colorsList.getSelectedIndex());
			colorsList.removeItemAt(colorsList.getSelectedIndex());
			age = 0;
			ageField.setBackground(Color.WHITE);
			nameField.setText("");
		}
		return players;
	}
	
	private JPanel getPanel() {
		JPanel mainPanel = new JPanel();
        JPanel topPanel = new JPanel();
        JPanel bottomPanel = new JPanel();
        JLabel nameLabel = new JLabel("Please enter your name:", 0);
        JLabel ageLabel = new JLabel("Enter your age [ 10 - 100 ]:", 0);
        JLabel colorLabel = new JLabel("Pick your color:", 0);
        nameField.setHorizontalAlignment(0);
        ageField.setHorizontalAlignment(0);
        mainPanel.setLayout(new GridLayout(2, 1, 50, 5));
        topPanel.setLayout(new GridLayout(2, 1));
        bottomPanel.setLayout(new GridLayout(2, 2, 30, 2));
        topPanel.add(nameLabel);
        topPanel.add(nameField);
        bottomPanel.add(ageLabel);
        bottomPanel.add(colorLabel);
        bottomPanel.add(ageField);
        bottomPanel.add(colorsList);
        mainPanel.add(topPanel);
        mainPanel.add(bottomPanel);
        return mainPanel;
	}
	
	public int getFirstPlayerIndex(Player[] allPlayers) {
		JComboBox<Player> comboBoxPlayers = new JComboBox<Player>(allPlayers);
		JPanel mainPanel = new JPanel();
		JLabel name = new JLabel("", 0);
		int youngestAge = allPlayers[0].age, index = 0, feedback;
		for(int i=1; i<allPlayers.length; i++)
			if (allPlayers[i].age < youngestAge) {
				youngestAge = allPlayers[i].age;
				index = i;
			}
		name.setText("Hey "+allPlayers[index].name+"!");
        mainPanel.setLayout(new GridLayout(3, 1, 5, 5));
        mainPanel.add(name);
        mainPanel.add(new JLabel("Seems like you are the youngest here. Would you mind to pick who will start this wonderful game?", 0));
        mainPanel.add(comboBoxPlayers);
        feedback = JOptionPane.showConfirmDialog(null, mainPanel, "One more thing...", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (feedback == JOptionPane.CANCEL_OPTION) System.exit(0);
		return comboBoxPlayers.getSelectedIndex();
	}
	
}
