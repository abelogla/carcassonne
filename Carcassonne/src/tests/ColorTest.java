package tests;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import code.GameManager;

public class ColorTest {

	@Test
	public void test01() {
		// Tested the Color Array attached to each Player
		
		GameManager gm = new GameManager();
		Color c0 = gm.getPlayerArray()[0].color;
		Color c1 = gm.getPlayerArray()[1].color;
		Color c2 = gm.getPlayerArray()[2].color;
		Color c3 = gm.getPlayerArray()[3].color;
		
		assertTrue("", c0.equals(Color.YELLOW));
		assertTrue("", c1.equals(Color.BLUE));
		assertTrue("", c2.equals(Color.RED));
		assertTrue("", c3.equals(Color.ORANGE));
	}

}
