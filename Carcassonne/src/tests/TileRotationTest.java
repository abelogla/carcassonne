package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Pointer;
import code.Terrain;
import code.Tile;

public class TileRotationTest {
// Tests the rotateTile method of Tile with random tiles
	
	@Test public void test01() {
		 // Road rotation
		String params = "1F000100001111C111000000000R000010010000F000001100000";
		Tile tile = new Tile(params, "someimage");
		Terrain road = null;
		for(Pointer p: tile.getAllPointersOnTile()){
			if((p.getTerrain().type).equals("Road")){
				road = p.getTerrain();
				
			}
		}
		tile.rotateTile();
		assertTrue(""+tile.getTerrainCoordinates(road), "000000010010".equals(tile.getTerrainCoordinates(road)));
	}
	
	@Test public void test02() {
		 // City rotation
		String params = "1F000100001111C111000000000R000010010000F000001100000";
		Tile tile = new Tile(params, "someimage");
		Terrain city = null;
		for(Pointer p: tile.getAllPointersOnTile()){
			if((p.getTerrain().type).equals("City")){
				city = p.getTerrain();
				// this is equals to "111000000000"
				
			}
		}
		tile.rotateTile();
		
		/* When that method is called the coordinates shift by 3 
		the coordinates should be equal "000111000000"
		*/		
		assertTrue(""+tile.getTerrainCoordinates(city), "000111000000".equals(tile.getTerrainCoordinates(city)));
	}
	
	// You should get the idea by now.


}
