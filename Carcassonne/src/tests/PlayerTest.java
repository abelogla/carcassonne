package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import code.GameBoard;
import code.GameManager;
import code.Player;

public class PlayerTest {

	@Test public void test01() {
		// Tests the contents of the PlayerName Array
		
		GameManager gm = new GameManager();
		String ai0 = gm.getPlayerArray()[0].name;
		String ai1 = gm.getPlayerArray()[1].name;
		String ai2 = gm.getPlayerArray()[2].name;
		String ai3 = gm.getPlayerArray()[3].name;

		assertTrue("", ai0.equals("Banana"));
		assertTrue("", ai1.equals("Carol"));
		assertTrue("", ai2.equals("Obama"));
		assertTrue("", ai3.equals("Jesus"));
	

}
}
