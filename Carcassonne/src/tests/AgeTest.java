package tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import code.GameManager;

public class AgeTest {
	@Test
	public void ageTest01(){
		//Test for age
		GameManager gm = new GameManager();
		int theAge0 = gm.getPlayerArray()[0].age;
		int theAge1 = gm.getPlayerArray()[1].age;
		int theAge2 = gm.getPlayerArray()[2].age;
		int theAge3 = gm.getPlayerArray()[3].age;
		
		assertTrue("", theAge0 == 18);
		assertTrue("", theAge1 == 45);
		assertTrue("", theAge2 == 65);
		assertTrue("", theAge3 == 30);
	}

	
}
